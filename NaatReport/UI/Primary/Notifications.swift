//
//  Notifications.swift
//  NaatReport
//
//  Created by Miyagui on 12/26/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

enum NotificationStatus: Int {
    case created = 1
    case completed = 3
    case reviewed = 5
}

class Notifications: BaseViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var aButton: UIButton!
    @IBOutlet weak var bButton: UIButton!
    @IBOutlet weak var cButton: UIButton!
    @IBOutlet weak var sectionIndicator: UIView!
    @IBOutlet weak var emptyNotifications: UIView!
    @IBOutlet weak var addNotificationButton: UIButton!
    
    @IBOutlet weak var notificationsTable: UITableView!
    
    var centinelaReportsInDraft = [NotificationReport]()
    var centinelaReportsSent = [NotificationReport]()
    var centinelaReportsSigned = [NotificationReport]()
    
    var request: DataRequest? = nil
    var idCentinelaPatient: String? = nil
    
    deinit {
        centinelaReportsInDraft.removeAll()
        centinelaReportsSent.removeAll()
        centinelaReportsSigned.removeAll()
        
        idCentinelaPatient = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationsTable.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        notificationsTable.separatorInset = .zero
        notificationsTable.tableFooterView = UIView()
        
        if let patient = DataBaseManager.get("CentinelaPatient", id: idCentinelaPatient) as? CentinelaPatient {
            let name = patient.name ?? ""
            
            let patientName = "\(name)"
            titleLabel.text = "Notificaciones: \(patientName)"
            backButton.isHidden = false
            addNotificationButton.isHidden = false
        }
        else if let appUsers = DataBaseManager.fetchObjects(objectName: "ApplicationUser") as? [ApplicationUser], let appUser = appUsers.first, let type = appUser.type, type == "patient" {
            addNotificationButton.isHidden = false
        }
        
        buildRefreshControl()
        selectA()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getReportsfFromWS()
    }
    
    func buildRefreshControl() {
        let refreshContainer = UIView()
        refreshContainer.backgroundColor = UIColor.lightGray
        refreshContainer.frame = .zero
        refreshContainer.tag = 100
        notificationsTable.addSubview(refreshContainer)
        
        let refreshControl = UIRefreshControl(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 10.0))
        refreshControl.tintColor = UIColor.Naat.orange
        refreshControl.addTarget(self, action: #selector(reload(_:)), for: .valueChanged)
        refreshControl.tag = 100
        refreshContainer.addSubview(refreshControl)
    }
    
    @objc func reload(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        if request != nil { return }
        
        notificationsTable.setVisible(false, animated: true, onComplete: { self.getReportsfFromWS()})
    }
    
    func updateTable() {
        notificationsTable.reloadData()
        
        if (aButton.isSelected && centinelaReportsInDraft.isEmpty == true) ||
            (bButton.isSelected && centinelaReportsSent.isEmpty == true) ||
            (cButton.isSelected && centinelaReportsSigned.isEmpty == true) {
            emptyNotifications.setVisible(true, animated: true, onComplete: nil)
        }
        else {
            emptyNotifications.setVisible(false, animated: true, onComplete: nil)
        }
        
        notificationsTable.setVisible(true, animated: true, onComplete: nil)
    }
    
    @IBAction func close() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectA() {
        aButton.isSelected = true
        bButton.isSelected = false
        cButton.isSelected = false
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
                        self.sectionIndicator.center.x = self.aButton.center.x
                        self.notificationsTable.alpha = 0.0
        }) { (finish) in
            self.updateTable()
        }
    }
    
    @IBAction func selectB() {
        aButton.isSelected = false
        bButton.isSelected = true
        cButton.isSelected = false
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
                        self.sectionIndicator.center.x = self.bButton.center.x
                        self.notificationsTable.alpha = 0.0
        }) { (finish) in
            self.updateTable()
        }
    }
    
    @IBAction func selectC() {
        aButton.isSelected = false
        bButton.isSelected = false
        cButton.isSelected = true
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
                        self.sectionIndicator.center.x = self.cButton.center.x
                        self.notificationsTable.alpha = 0.0
        }) { (finish) in
            self.updateTable()
        }
    }
    
    @IBAction func requestNewReport() {
        self.performSegue(withIdentifier: "AddNotification", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewReport = segue.destination as? ViewReport, let formInReport = sender as? FormInReport {
            viewReport.idFormInReport = formInReport.idFormInReport
        }
        else if let selectType = segue.destination as? SelectEntryType {
            selectType.idCentinelaPatient = idCentinelaPatient
        }
        else if let formPicker = segue.destination as? FormPicker, let report = sender as? CentinelaReport {
            formPicker.idCentinelaReport = report.idCentinelaReport
        }
    }
}

extension Notifications {
    func getReportsfFromWS() {
        var url = "\(API)"
        
        if let idCentinelaPatient = self.idCentinelaPatient {
            url += "/patients/\(idCentinelaPatient)/notifications"
        }
        else {
            url += "/notifications"
        }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString { (response) in
            switch response.result {
            case .success (let string):
                if self.reviewTokenConflict(string) { return }
                
                guard let notificationsInfo = string.json() as? [[AnyHashable:Any]] else { return }
                var filter = ""
                if let idCentinelaPatient = self.idCentinelaPatient {
                    filter = "centinelaPatient.idCentinelaPatient LIKE[c] '\(idCentinelaPatient)'"
                }
                
                if let currentReports = DataBaseManager.fetchObjects(objectName: "NotificationReport", filter: filter, sortingMethods: [], context: DataBaseManager.context()) as? [NotificationReport] {
                    for existingReport in currentReports {
                        var found = false
                        
                        for info in notificationsInfo {
                            guard
                                let idNotificationReport = info["idNotificationReport"] as? [AnyHashable:Any],
                                let idNotification = (idNotificationReport["idNotificationReport"] as? NSNumber)?.intValue,
                                let idReport =  (idNotificationReport["idReport"] as? NSNumber)?.intValue
                                else {
                                    continue
                            }
                            
                            let id = "\(idNotification),\(idReport)"
                            if (existingReport.idNotificationReport)! == id {
                                found = true
                                break
                            }
                        }
                        
                        if !found {
                            DataBaseManager.context().delete(existingReport)
                        }
                    }
                }
                
                for info in notificationsInfo {
                    DataBaseManager.updateNotificationReport(info)
                }
                
                self.updateNotifications()
                
                break
            case .failure:
                break
            }
        }
    }
    
    func updateNotifications() {
        centinelaReportsInDraft.removeAll()
        centinelaReportsSent.removeAll()
        centinelaReportsSigned.removeAll()
        
        var filter = ""
        if let idCentinelaPatient = self.idCentinelaPatient {
            filter = "centinelaPatient.idCentinelaPatient LIKE '\(idCentinelaPatient)' AND "
        }
        
        if let newDraftNotifications = DataBaseManager.fetchObjects(objectName: "NotificationReport", filter: "\(filter) centinelaReport.status = \(NotificationStatus.created.rawValue)", sortingMethods: ["centinelaReport.idCentinelaReport"], context: DataBaseManager.context()) as? [NotificationReport] {
            centinelaReportsInDraft.append(contentsOf: newDraftNotifications.reversed())
        }
        
        if let newSentNotifications = DataBaseManager.fetchObjects(objectName: "NotificationReport", filter: "\(filter) centinelaReport.status = \(NotificationStatus.completed.rawValue)", sortingMethods: ["centinelaReport.idCentinelaReport"], context: DataBaseManager.context()) as? [NotificationReport] {
            centinelaReportsSent.append(contentsOf: newSentNotifications.reversed())
        }
        
        if let newSignedNotifications = DataBaseManager.fetchObjects(objectName: "NotificationReport", filter: "\(filter) centinelaReport.status = \(NotificationStatus.reviewed.rawValue)", sortingMethods: ["centinelaReport.idCentinelaReport"], context: DataBaseManager.context()) as? [NotificationReport] {
            centinelaReportsSigned.append(contentsOf: newSignedNotifications.reversed())
        }
        
        updateTable()
    }
    
    func getNotification(at indexPath: IndexPath) -> NotificationReport? {
        var report: NotificationReport? = nil
        
        if aButton.isSelected {
            report = centinelaReportsInDraft[indexPath.row]
        }
        else if bButton.isSelected {
            report = centinelaReportsSent[indexPath.row]
        }
        else if cButton.isSelected {
            report = centinelaReportsSigned[indexPath.row]
        }
        
        return report
    }
}

extension Notifications: UITableViewDelegate, UITableViewDataSource {
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if aButton.isSelected {
            return centinelaReportsInDraft.count
        }
        else if bButton.isSelected {
            return centinelaReportsSent.count
        }
        else if cButton.isSelected {
            return centinelaReportsSigned.count
        }
        
        return 0
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return idCentinelaPatient != nil ? 100 : 130
    }
    
    internal func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    internal func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .normal, title: "Retro-alimentar") { (action, indexPath) in
            //let patient = self.patients[indexPath.row]
            //self.performSegue(withIdentifier: "AddEditPatient", sender: patient)
        }
        
        editAction.backgroundColor = UIColor.Naat.blue
        
        return [editAction]
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationTableViewCell
        cell.selectionStyle = .none
        
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let notificationCell = cell as? NotificationTableViewCell else { return }
        
        var color = UIColor.Naat.orange
        var reportName = ""
        var productName = ""
        var patientName = ""
        var status = ""
        var icon = ""
        var date: Date? = Date()
        
        if let notification = getNotification(at: indexPath) {
            productName = notification.centinelaMedicine?.name ?? notification.centinelaProduct?.name ?? notification.centinelaProduct?.commercialName ?? "Producto"
            patientName = notification.centinelaPatient?.name ?? notification.appUser?.username ?? "Paciente"
            reportName = notification.centinelaReport?.name ?? "Reporte"
            date = notification.centinelaReport?.dateRegister ?? Date()
            
            if let origin = notification.centinelaReport?.origin {
                if origin == "application" {
                    icon = ""
                }
            }
        }
        
        if aButton.isSelected {
            status = "Comenzado"
            color = UIColor.darkGray
        }
        else if bButton.isSelected {
            status = "Enviado"
            color = UIColor.Naat.blue
        }
        else if cButton.isSelected {
            status = "Verificado"
            color = UIColor.Naat.orange
        }
        
        notificationCell.reportNameLabel.text = reportName
        notificationCell.productNameLabel.text = productName
        notificationCell.statusLabel.text = status
        notificationCell.statusLabel.textColor = color
        notificationCell.dateLabel.text = date?.stringInFormat(.slashedFullSeconds)
        notificationCell.nextIndicator.isHidden = (aButton.isSelected == false)
        notificationCell.kindIcon.text = icon
        
        if (idCentinelaPatient == nil) {
            notificationCell.patientLabel.isHidden = false
            notificationCell.patientNameLabel.isHidden = false
            notificationCell.patientNameLabel.text = patientName
        }
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let notification = getNotification(at: indexPath)
        let report = notification?.centinelaReport
        
        let appUsers = DataBaseManager.fetchObjects(objectName: "ApplicationUser") as? [ApplicationUser]
        let centinelaUsers = DataBaseManager.fetchObjects(objectName: "CentinelaUser") as? [CentinelaUser]
        
        if (centinelaUsers?.first) != nil {
            performSegue(withIdentifier: "PickForm", sender: report)
        }
        else if let appUser = appUsers?.first {
            if appUser.type == "doctor" {
                performSegue(withIdentifier: "PickForm", sender: report)
            }
            else if let firstForm = report?.forms?.allObjects.first as? FormInReport {
                performSegue(withIdentifier: "ViewReport", sender: firstForm)
            }
        }
    }
}
