//
//  FormPicker.swift
//  NaatReport
//
//  Created by Miyagui on 2/22/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class FormPicker: BaseViewController {
    @IBOutlet weak var formTable: UITableView!
    @IBOutlet weak var loadingIndicator: CircularActivityIndicator!
    
    final let formCellID = "FormCellID"
    
    lazy var availableForms = [(Int, String)]()
    lazy var formsAlreadyUsed = [FormInReport]()
    
    var idCentinelaReport: String? = nil
    
    deinit {
        idCentinelaReport = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formTable.register(UINib(nibName: "FormTableViewCell", bundle: nil), forCellReuseIdentifier: formCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestForms()
    }
    
    func requestForms() {
        guard let idReport = idCentinelaReport, let report = DataBaseManager.get("CentinelaReport", id: idReport) as? CentinelaReport else { return }
        
        formsAlreadyUsed.removeAll()
        availableForms.removeAll()
        
        formTable.setVisible(false, animated: true, onComplete: {
            self.loadingIndicator.startInderminateAnimation()
            
            Alamofire.request("\(API)/report/\(idReport)/forms", method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString { (response) in
                self.loadingIndicator.stopInderminateAnimation()
                
                switch response.result {
                case .success (let string):
                    if self.reviewTokenConflict(string) { return }
                    
                    guard let reportInfo = string.json() as? [AnyHashable:Any], reportInfo.keys.contains("available") else { return }
                    
                    if let formsInReport = reportInfo["used"] as? [[AnyHashable:Any]] {
                        for fir in formsInReport {
                            if let usedForm = DataBaseManager.updateFormInReport(fir, report: report) {
                                self.formsAlreadyUsed.append(usedForm)
                            }
                        }
                    }
                    
                    if let af = reportInfo["available"] as? [[AnyHashable:Any]] {
                        for form in af {
                            guard let idForm = (form["id"] as? NSNumber)?.intValue, let name = form["name"] as? String else { continue }
                            self.availableForms.append((idForm, name))
                        }
                    }
                    break
                case .failure:
                    break
                }
                
                self.requestEnded()
            }
        })
    }
    
    func requestEnded() {
        formTable.reloadData()
        loadingIndicator.stopInderminateAnimation()
        formTable.setVisible(true, animated: true, onComplete: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewForm = segue.destination as? ViewReport, let formInReport = sender as? FormInReport {
            viewForm.idFormInReport = formInReport.idFormInReport
        }
    }
    
    @IBAction func close() {
        navigationController?.popViewController(animated: true)
    }
}

extension FormPicker: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return availableForms.count
        }
        
        return formsAlreadyUsed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: formCellID, for: indexPath) as! FormTableViewCell
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let formCell = cell as? FormTableViewCell else { return }
        
        if indexPath.section == 0 {
            let entry = availableForms[indexPath.row]
            formCell.formNameLabel.text = entry.1
            formCell.formAddButton.isHidden = false
            formCell.selectionStyle = .none
        }
        else if indexPath.section == 1 {
            let entry = formsAlreadyUsed[indexPath.row]
            formCell.formNameLabel.text = entry.file ?? ""
            formCell.formAddButton.isHidden = true
            formCell.selectionStyle = .default
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1 {
            let entry = formsAlreadyUsed[indexPath.row]
            self.performSegue(withIdentifier: "ViewForm", sender: entry)
        }
    }
}

extension FormPicker: FormTableViewCellDelegate {
    func formRequestToAdd(_ cell: FormTableViewCell) {
        if let indexPath = formTable.indexPath(for: cell), let idCentinelaReport = self.idCentinelaReport, let idReport = Int(idCentinelaReport), let report = DataBaseManager.get("CentinelaReport", id: idCentinelaReport) as? CentinelaReport {
            let entry = availableForms[indexPath.row]
            formTable.setVisible(false, animated: true, onComplete: {
                self.loadingIndicator.startInderminateAnimation()
                
                Alamofire.request("\(API)/addFormToReport", method: .post, parameters: ["idForm":entry.0,"idReport":idReport], encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
                    switch response.result {
                    case .success (let string):
                        if self.reviewTokenConflict(string) { return }
                        
                        guard let firInfo = string.json() as? [AnyHashable:Any], firInfo.keys.contains("id") else { return }
                        
                        if let formInReport = DataBaseManager.updateFormInReport(firInfo, report: report) {
                            let targetIndex = self.formsAlreadyUsed.count
                            self.availableForms.remove(at: indexPath.row)
                            self.formsAlreadyUsed.append(formInReport)
                            
                            self.formTable.beginUpdates()
                            self.formTable.deleteRows(at: [indexPath], with: .top)
                            self.formTable.insertRows(at: [IndexPath(row: targetIndex, section: 1)], with: .top)
                            self.formTable.endUpdates()
                        }
                        break
                    case .failure:
                        break
                    }
                })
                
                self.requestEnded()
            })
        }
    }
}
