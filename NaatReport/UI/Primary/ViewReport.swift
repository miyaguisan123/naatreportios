//
//  ViewReport.swift
//  NaatReport
//
//  Created by Miyagui on 12/26/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

class ViewReport: UIViewController {
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var loadingIndicator: CircularActivityIndicator!
    
    var idFormInReport: String? = nil
    
    deinit {
        idFormInReport = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlString = "\(API)/viewForm.htm"
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.setValue(UtilitySession.getSessionToken(), forHTTPHeaderField: "token")
        request.setValue(idFormInReport, forHTTPHeaderField: "idFormInReport")
        
        webview.scrollView.delegate = self
        webview.scrollView.isScrollEnabled = false
        webview.scrollView.bounces = false
        webview.loadRequest(request as URLRequest)
        
        loadingIndicator.startInderminateAnimation()
    }
    
    func handleAction(_ action: String) {
        if action == "back" {
            close()
        }
    }
    
    @IBAction func close() {
        view.endEditing(true)
        let appUsers = DataBaseManager.fetchObjects(objectName: "ApplicationUser") as? [ApplicationUser]
        var target = navigationController?.viewControllers[2]
        
        if target?.isEqual(self) == true {
            navigationController?.popViewController(animated: true)
        }
        else {
            if (appUsers?.first) != nil {
                target = navigationController?.viewControllers[1]
            }
            
            navigationController?.popToViewController(target!, animated: true)
        }
    }
}

extension ViewReport: UIScrollViewDelegate, UIWebViewDelegate {
    internal func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    internal func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingIndicator.stopInderminateAnimation()
        webview.alpha = 0.0
        webview.isHidden = false
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
                        self.webview.alpha = 1.0
        }, completion: nil)
    }
    
    internal func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        guard let url = request.url?.absoluteString else { return true }
        
        if url.hasPrefix("app://") {
            var action = url as NSString
            action = action.substring(from: 6) as NSString
            handleAction(action as String)
            
            return false
        }
        
        return true
    }
}
