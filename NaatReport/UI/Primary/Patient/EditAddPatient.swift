//
//  EditAddPatient.swift
//  NaatReport
//
//  Created by Miyagui on 12/15/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class EditAddPatient: BaseViewController {
    @IBOutlet weak var contentScroll: UIScrollView!
    
    @IBOutlet weak var avatar: UIButton!
    
    @IBOutlet weak var nameField: CustomUITextField!
    @IBOutlet weak var surnameFather: CustomUITextField!
    @IBOutlet weak var surnameMother: CustomUITextField!
    @IBOutlet weak var rfc: CustomUITextField!
    
    @IBOutlet weak var dateField: CustomUITextField!
    @IBOutlet weak var curp: CustomUITextField!
    @IBOutlet weak var phone: CustomUITextField!
    @IBOutlet weak var email: CustomUITextField!
    @IBOutlet weak var postalCode: CustomUITextField!
    @IBOutlet weak var locationField: CustomUITextField!
    @IBOutlet weak var neighborhood: CustomUITextField!
    @IBOutlet weak var neighborhoodPicker: UIPickerView!
    
    @IBOutlet weak var genderFButton: UIButton!
    @IBOutlet weak var genderMButton: UIButton!
    
    var idCentinelaPatient: String? = nil {
        didSet {
            if let patient = DataBaseManager.get("CentinelaPatient", id: idCentinelaPatient) as? CentinelaPatient {
                birthDate = patient.dateBirth
            }
        }
    }
    
    var birthDate: Date? = nil {
        didSet {
            if birthDate != nil && dateField != nil {
                dateField.text = birthDate?.stringInFormat(.slashedNumericStandard)
            }
        }
    }
    
    lazy var neighborhoods = [(Int,String)]()
    var idCity = 0
    var idState = 0
    var idHood = 0
    
    deinit {
        neighborhoods.removeAll()
        birthDate = nil
        idCentinelaPatient = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentScroll.contentSize.height = (contentScroll.subviews.last?.bottom)! + 30.0
        
        if let patient = DataBaseManager.get("CentinelaPatient", id: idCentinelaPatient) as? CentinelaPatient {
            nameField.text = patient.name
            surnameFather.text = patient.lastnameFather
            surnameMother.text = patient.lastnameMother
            rfc.text = patient.rfc
            genderFButton.isSelected = (patient.gender == false)
            genderMButton.isSelected = (patient.gender == true)
            dateField.text = patient.dateBirth?.stringInFormat(.slashedNumericStandard)
            
            curp.text = patient.curp
            phone.text = patient.phone
            postalCode.text = "\(patient.zipCode ?? 0)"
            
            let city = patient.cityName ?? ""
            let state = patient.stateName ?? ""
            let location = "\(city), \(state)".trimmingCharacters(in: .whitespaces)
            
            idCity = patient.idCity?.intValue ?? 0
            idState = patient.idState?.intValue ?? 0
            idHood = patient.idHood?.intValue ?? 0
            
            locationField.text = location
            neighborhood.text = patient.hoodName
            
            var innitials = (patient.name?.isEmpty == false) ? String((patient.name?.first)!) : ""
            if patient.lastnameFather?.isEmpty == false {
                innitials += String((patient.lastnameFather?.first)!)
            }
            if patient.lastnameMother?.isEmpty == false {
                innitials += String((patient.lastnameMother?.first)!)
            }
            
            avatar.setTitle(innitials, for: .normal)
            requestLocationData()
        }
        
        avatar.clipsToBounds = true
        avatar.layer.cornerRadius = avatar.bounds.height / 2.0
        avatar.layer.borderColor = UIColor.Naat.orange.cgColor
        avatar.layer.borderWidth = 1.0
        
        neighborhoodPicker.frame.origin.y = view.bounds.height
    }
    
    func updatePatientInnitials() {
        let name = (nameField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true) ? "" : String((nameField.text?.first)!)
        let lastnameFather = (surnameFather.text?.trimmingCharacters(in: .whitespaces).isEmpty == true) ? "" : String((surnameFather.text?.first)!)
        let lastnameMother = (surnameMother.text?.trimmingCharacters(in: .whitespaces).isEmpty == true) ? "" : String((surnameMother.text?.first)!)
        
        avatar.setTitle("\(name) \(lastnameFather) \(lastnameMother)", for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func dismissAction() {
        nameField.resignFirstResponder()
        surnameFather.resignFirstResponder()
        surnameMother.resignFirstResponder()
        rfc.resignFirstResponder()
        
        curp.resignFirstResponder()
        phone.resignFirstResponder()
        postalCode.resignFirstResponder()
        
        dismissPicker()
    }
    
    func validateFields() -> Bool {
        var formIsValid = true
        
        for view in contentScroll.subviews {
            guard let field = view as? CustomUITextField else { continue }
            
            if field.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
                field.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                formIsValid = false
            }
        }
        
        if genderFButton.isSelected == false && genderMButton.isSelected == false {
            genderFButton.setTitleColor(UIColor.red, for: .normal)
            genderMButton.setTitleColor(UIColor.red, for: .normal)
            formIsValid = false
        }
        
        let wantedRFC = rfc.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if wantedRFC == nil || wantedRFC?.isValidRFC() == false {
            rfc.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
            
            showAlert(title: "RFC Inválido", message: "El RFC que ingresó no es válido.")
        }
        else if let validRFC = wantedRFC, curp.text?.hasPrefix(validRFC) == false {
            curp.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
            
            showAlert(title: "CURP Inválido", message: "El CURP que ingresó no es válido.")
        }
        
        if formIsValid == false {
            showAlert(title: "Campos Obligatorios", message: "Todos los campos son obligatorios.")
        }
        
        return formIsValid
    }
    
    func dismissPicker() {
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.neighborhoodPicker.frame.origin.y = self.view.frame.size.height
                        self.contentScroll.frame.size.height = self.neighborhoodPicker.top
        }, completion: nil)
    }
    
    @IBAction func close() {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func takePhoto() {
        addContent(from: .camera, sourceRect: .zero)
    }
    
    @IBAction func requestBirthDate() {
        view.endEditing(true)
        
        let userCalendar = Calendar.current
        var minimumBirthDate = DateComponents()
        minimumBirthDate.year = 1920
        minimumBirthDate.month = 1
        minimumBirthDate.day = 1
        
        let ds = DateSelector()
        ds.modalPresentationStyle = .overCurrentContext
        ds.minDate = userCalendar.date(from: minimumBirthDate)!
        ds.selectedDate = birthDate ?? Date()
        ds.delegate = self
        
        present(ds, animated: false, completion: nil)
    }
    
    @IBAction func setGender(_ sender: UIButton) {
        dismissAction()
        sender.isSelected = true
        
        if sender.tag == 0 {
            genderMButton.isSelected = false
        }
        else {
            genderFButton.isSelected = false
        }
        
        genderFButton.setTitleColor(UIColor.Naat.blue, for: .normal)
        genderMButton.setTitleColor(UIColor.Naat.blue, for: .normal)
    }
    
    @IBAction func createPatient() {
        guard validateFields() == true else { return }
        
        var params = [String:Any]()
        params["name"] = nameField.text
        params["father_lastname"] = surnameFather.text
        params["mother_lastname"] = surnameMother.text
        params["birth_date"] = birthDate?.stringInFormat(.slashedNumericStandard)
        params["rfc"] = rfc.text?.uppercased()
        params["gender"] = genderMButton.isSelected
        
        params["curp"] = curp.text
        params["phone"] = phone.text
        params["email"] = email.text
        params["zipCode"] = postalCode.text
        params["idHood"] = idHood
        params["idCity"] = idCity
        params["idState"] = idState
        
        if let idCentinelaPatient = self.idCentinelaPatient {
            Alamofire.request("\(API)/patients/\(idCentinelaPatient)", method: .put, parameters: params, encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
                switch response.result {
                case .success(let string):
                    if self.reviewTokenConflict(string) { return }
                    
                    guard let patientInfo = string.json() as? [AnyHashable:Any], patientInfo.keys.contains("id") else {
                        self.networkRequestFailed()
                        return
                    }
                    
                    SystemMessage(title: "Información Actualizada", message: "")
                    DataBaseManager.updateCentinelaPatient(patientInfo)
                    
                    break
                case .failure:
                    self.networkRequestFailed()
                    break
                }
            })
        }
        else {
            Alamofire.request("\(API)/addPatient", method: .post, parameters: params, encoding: JSONEncoding(), headers: UtilitySession.defaultHeaders()).responseString { (response) in
                switch response.result {
                case .success(let string):
                    if self.reviewTokenConflict(string) { return }
                    
                    guard let patientInfo = string.json() as? [AnyHashable:Any] else {
                        self.networkRequestFailed()
                        return
                    }
                    
                    if patientInfo.keys.contains("id") {
                        DataBaseManager.updateCentinelaPatient(patientInfo)
                        self.close()
                    }
                    else if let error = patientInfo["error"] as? String {
                        SystemMessage(title: "Error", message: error)
                    }
                    
                    break
                case .failure:
                    self.networkRequestFailed()
                    break
                }
            }
        }
    }
    
    @IBAction func selectNeighborhood() {
        guard neighborhoodPicker.top == view.frame.size.height, neighborhoods.isEmpty == false else { return }
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.neighborhoodPicker.frame.origin.y = self.view.frame.size.height - self.neighborhoodPicker.frame.size.height
                        self.contentScroll.frame.size.height = self.neighborhoodPicker.top
                        self.contentScroll.contentOffset.y += self.neighborhoodPicker.bounds.height
        }, completion: nil)
    }
}

extension EditAddPatient {
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            if idCentinelaPatient == nil {
                nameField.text = "Angélica"
                surnameFather.text = "Rivera"
                surnameMother.text = "Hurtado"
                
                dateField.text = "02/08/1969"
                rfc.text = "RHA690802HMN"
                genderFButton.isSelected = true
                genderMButton.isSelected = false
                
                let userCalendar = Calendar.current
                var riveraBirthDate = DateComponents()
                riveraBirthDate.year = 1969
                riveraBirthDate.month = 8
                riveraBirthDate.day = 2
                
                birthDate = userCalendar.date(from: riveraBirthDate)
            }
        }
    }
}

extension EditAddPatient: DateSelectorDelegate {
    internal func dateSelected(_ date: Date?) {
        birthDate = date
    }
}

extension EditAddPatient: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(curp) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 18
        }
        else if textField.isEqual(postalCode) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 5
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
        dismissPicker()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
            textField.text = ""
        }
        
        if textField.text?.isEmpty == true {
            textField.resignFirstResponder()
        }
        else {
            if textField.isEqual(nameField) {
                surnameFather.becomeFirstResponder()
            }
            else if textField.isEqual(surnameFather) {
                surnameMother.becomeFirstResponder()
            }
            else if textField.isEqual(surnameMother) {
                rfc.becomeFirstResponder()
            }
            else if textField.isEqual(rfc) {
                textField.resignFirstResponder()
            }
            else if textField.isEqual(email) {
                curp.becomeFirstResponder()
            }
            else if textField.isEqual(curp) {
                phone.becomeFirstResponder()
            }
            else if textField.isEqual(phone) {
                postalCode.becomeFirstResponder()
            }
            else {
                textField.resignFirstResponder()
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
        updatePatientInnitials()
        
        if textField.isEqual(postalCode) {
            requestLocationData()
        }
        
        if textField.isEqual(rfc) {
            textField.text = textField.text?.uppercased()
        }
    }
}

extension EditAddPatient {
    func requestLocationData() {
        guard let zipCode = postalCode.text, zipCode.trimmingCharacters(in: .whitespaces).isEmpty == false else { return }
        
        _ = Alamofire.request("\(API)/location?zip=\(zipCode)", method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
            switch response.result {
            case .success(let string):
                guard let locationData = string.json() as? [AnyHashable:Any] else { return }
                
                self.neighborhoods.removeAll()
                if let n = locationData["neighborhoods"] as? [[AnyHashable:Any]] {
                    for entry in n {
                        let name = entry["name"] as? String ?? ""
                        let id = (entry["id"] as? NSNumber)?.intValue ?? 0
                        
                        self.neighborhoods.append((id, name))
                    }
                    
                    self.neighborhood.text = self.neighborhoods.first?.1
                    self.idHood = self.neighborhoods.first?.0 ?? 0
                }
                
                var cityName = ""
                var stateName = ""
                if let city = locationData["city"] as? [AnyHashable:Any] {
                    let name = city["name"] as? String ?? ""
                    let id = (city["id"] as? NSNumber)?.intValue ?? 0
                    
                    cityName = name
                    self.idCity = id
                }
                
                if let state = locationData["state"] as? [AnyHashable:Any] {
                    let name = state["name"] as? String ?? ""
                    let id = (state["id"] as? NSNumber)?.intValue ?? 0
                    
                    stateName = name
                    self.idState = id
                }
                
                self.locationField.text = "\(cityName), \(stateName)"
                self.locationField.set(mode: .normal, defaultBackgroundColor: UIColor("#DDDDDD"), defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                self.locationField.textColor = UIColor("#707070")
                
                break
            case .failure:
                break
            }
            
            self.neighborhoodPicker.reloadAllComponents()
        })
    }
}

extension EditAddPatient: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return neighborhoods.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return neighborhoods[row].1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let entry = neighborhoods[row]
        neighborhood.text = entry.1
        idHood = entry.0
        
        neighborhood.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}
