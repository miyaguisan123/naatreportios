//
//  Login.swift
//  NaatReport
//
//  Created by System on 11/21/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class Login: BaseViewController {
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var usernameField: CustomUITextField!
    @IBOutlet weak var passwordField: CustomUITextField!
    @IBOutlet weak var loginButton: NaatButton!
    
    @IBOutlet weak var buildLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: CircularActivityIndicator!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var versionNumber = Bundle.main.releaseVersionNumber
        if versionNumber == nil {
            versionNumber = "1.0.0"
        }
        
        loginButton.normalBackgroundColor = UIColor.Naat.lightOrange
        loginButton.highlightBackgroundColor = UIColor.Naat.orange
        
        buildLabel.text = "versión \(versionNumber!)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        usernameField.text = nil
        passwordField.text = nil
        contentScroll.setVisible(true, animated: true, onComplete: nil)
        
        UtilitySession.setSessionVar(key: "SEEN_PRIVACY_MESSAGE", value: "NO")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func keyboardWillShow(height: CGFloat, animationDuration: TimeInterval) {
        self.buildLabel.setVisible(false, animated: true, onComplete: nil)
        UIView.animate(withDuration: animationDuration) {
            var transform = CGAffineTransform(translationX: 0.0, y: -80.0)
            transform = transform.scaledBy(x: 0.5, y: 0.5)
            self.logo.transform = transform
        }
    }
    
    override func keyboardWillHide(animationDuration: TimeInterval) {
        self.buildLabel.setVisible(true, animated: true, onComplete: nil)
        UIView.animate(withDuration: animationDuration) {
            self.logo.transform = CGAffineTransform.identity
        }
    }
    
    override func dismissAction() {
        view.endEditing(true)
    }
    
    func restoreView() {
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       animations: {
                        self.contentScroll.alpha = 1.0
                        self.activityIndicator.stopInderminateAnimation()
        })
    }
    
    @IBAction func tryLogin() {
        if usernameField.text?.isEmpty == false && passwordField.text?.isEmpty == false {
            view.endEditing(true)
            view.isUserInteractionEnabled = false
            
            self.activityIndicator.startInderminateAnimation()
            contentScroll.setVisible(false, animated: true, onComplete: { self.loginUser() })
        }
        else if usernameField.text?.isEmpty == true {
            usernameField.becomeFirstResponder()
        }
        else if passwordField.text?.isEmpty == true {
            passwordField.becomeFirstResponder()
        }
    }
}

extension Login {
    func loginUser() {
        DataBaseManager.deleteAllDataFromModel("ApplicationUser")
        DataBaseManager.deleteAllDataFromModel("CentinelaUser")
        
        guard let username = usernameField.text, let password = passwordField.text, username.trimmingCharacters(in: .whitespaces).isEmpty == false, password.trimmingCharacters(in: .whitespaces).isEmpty == false else { return }
        
        Alamofire.request("\(API)/login", method: .post, parameters: ["username":username,"password":password], encoding: JSONEncoding.default, headers: API_DEFAULT_HEADERS).responseString(completionHandler: { (response) in
            switch response.result {
            case .failure:
                self.loginFailed()
                break
            case .success (let string):
                if self.reviewTokenConflict(string) { return }
                
                guard
                    let loginInfo = string.json() as? [AnyHashable:Any],
                    let token = loginInfo["token"] as? String,
                    let userInfo = loginInfo["user"] as? [AnyHashable:Any],
                    let type = loginInfo["type"] as? String
                    else {
                        self.loginFailed(false)
                        return
                }
                
                UtilitySession.setSessionToken(token)
                if type == "Centinela" {
                    DataBaseManager.updateCentinelaUser(userInfo)
                }
                else {
                    DataBaseManager.updateApplicationUser(userInfo)
                }
                
                self.loginSuccess()
                
                break
            }
        })
    }
    
    func loginFailed(_ networkError: Bool = true) {
        view.isUserInteractionEnabled = true
        contentScroll.isHidden = false
        restoreView()
        
        self.passwordField.text = ""
        if networkError == false {
            self.showAlert(title: "Datos incorrectos", message: "El usuario o password ingresados es incorrecto.")
        }
        else {
            self.networkRequestFailed()
        }
    }
    
    func loginSuccess() {
        let appUsers = DataBaseManager.fetchObjects(objectName: "ApplicationUser") as? [ApplicationUser]
        let centinelaUsers = DataBaseManager.fetchObjects(objectName: "CentinelaUser") as? [CentinelaUser]
        
        if (centinelaUsers?.first) != nil {
            performSegue(withIdentifier: "ProfessionalHome", sender: nil)
        }
        else if let appUser = appUsers?.first {
            if appUser.type == "doctor" {
                performSegue(withIdentifier: "ProfessionalHome", sender: nil)
            }
            else {
                performSegue(withIdentifier: "PatientHome", sender: nil)
            }
        }
    }
}

extension Login: UITextFieldDelegate {
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(usernameField) {
            passwordField.becomeFirstResponder()
        }
        else if textField.isEqual(passwordField) {
            tryLogin()
        }
        
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}
