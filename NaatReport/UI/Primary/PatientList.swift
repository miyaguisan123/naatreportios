//
//  PatientList.swift
//  NaatReport
//
//  Created by Miyagui on 11/23/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class PatientList: BaseViewController {
    @IBOutlet weak var patientTable: UITableView!
    @IBOutlet weak var searchField: CustomUITextField!
    @IBOutlet weak var searchClearButton: UIButton!
    @IBOutlet weak var emptyPatientList: UIView!
    
    @IBOutlet weak var clearButtonWidthConstraint: NSLayoutConstraint!
    
    var request: DataRequest? = nil
    let patientCellID = "PatientCell"
    public var patients = [CentinelaPatient]()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        patientTable.register(UINib(nibName: "PatientTableViewCell", bundle: nil), forCellReuseIdentifier: patientCellID)
        patientTable.separatorInset = .zero
        patientTable.tableFooterView = UIView()
        
        buildRefreshControl()
        
        UITabBar.appearance().barTintColor = UIColor.Naat.dark
        UITabBar.appearance().backgroundColor = UIColor.Naat.dark
    }
    
    func buildRefreshControl() {
        let refreshContainer = UIView()
        refreshContainer.backgroundColor = UIColor.lightGray
        refreshContainer.frame = .zero
        refreshContainer.tag = 100
        patientTable.addSubview(refreshContainer)
        
        let refreshControl = UIRefreshControl(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 10.0))
        refreshControl.tintColor = UIColor.Naat.orange
        refreshControl.addTarget(self, action: #selector(reload(_:)), for: .valueChanged)
        refreshControl.tag = 100
        refreshContainer.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getPatientsFromWS()
    }
    
    @objc func reload(_ refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        if request != nil { return }
        
        getPatientsFromWS()
    }
    
    @IBAction func filterResults() {
        view.endEditing(true)
        patients.removeAll()
        
        var results: [CentinelaPatient]? = nil
        if searchField.text?.isEmpty == false {
            clearButtonWidthConstraint.constant = 35.0
            view.updateLayout(animated: true)
            
            results = DataBaseManager.fetchObjects(objectName: "CentinelaPatient", filter: "name CONTAINS[cd] '\((searchField.text)!)' OR lastnameFather CONTAINS[cd] '\((searchField.text)!)' OR lastnameMother CONTAINS[cd] '\((searchField.text)!)'") as? [CentinelaPatient]
        }
        else {
            clearButtonWidthConstraint.constant = 0.0
            view.updateLayout(animated: true)
            results = DataBaseManager.fetchObjects(objectName: "CentinelaPatient") as? [CentinelaPatient]
        }
        
        if results != nil {
            patients += results!
        }
        
        patientTable.reloadData()
    }
    
    @IBAction func clearFilter() {
        searchField.text = ""
        searchField.resignFirstResponder()
        filterResults()
        
        clearButtonWidthConstraint.constant = 0.0
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func addNewPatient() {
        view.endEditing(true)
        performSegue(withIdentifier: "AddEditPatient", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddEditPatient" && sender != nil {
            guard let ep = segue.destination as? EditAddPatient, let patient = sender as? CentinelaPatient else { return }
            ep.idCentinelaPatient = patient.idCentinelaPatient
        }
        else if segue.identifier == "ViewNotifications" && sender != nil {
            guard let vn = segue.destination as? Notifications, let patient = sender as? CentinelaPatient else { return }
            vn.idCentinelaPatient = patient.idCentinelaPatient
        }
    }
}

extension PatientList {
    func getPatientsFromWS() {
        request = Alamofire.request("\(API)/patients", method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
            self.request = nil
            switch response.result {
            case .success (let string):
                if self.reviewTokenConflict(string) { return }
                
                guard let patientsInfo = string.json() as? [[AnyHashable:Any]] else { return }
                
                if let currentPatients = DataBaseManager.fetchObjects(objectName: "CentinelaPatient", filter: nil, sortingMethods: [], context: DataBaseManager.context()) as? [CentinelaPatient] {
                    for existingPatient in currentPatients {
                        var found = false
                        for info in patientsInfo {
                            guard let idCentinelaPatient = DataBaseManager.getId(from: info["id"]) else { continue }
                            if idCentinelaPatient == (existingPatient.idCentinelaPatient)! {
                                found = true
                                break
                            }
                        }
                        
                        if !found {
                            DataBaseManager.context().delete(existingPatient)
                        }
                    }
                }
                
                for info in patientsInfo {
                    DataBaseManager.updateCentinelaPatient(info)
                }
                
                break
            case .failure:
                break
            }
            
            self.updatePatients()
        })
    }
    
    func updatePatients() {
        patients.removeAll()
        if let currentPatients = DataBaseManager.fetchObjects(objectName: "CentinelaPatient", filter: nil, sortingMethods: ["idCentinelaPatient"], context: DataBaseManager.context()) as? [CentinelaPatient] {
            patients.append(contentsOf: currentPatients)
        }
        
        DispatchQueue.main.async {
            self.patientTable.reloadData()
            self.emptyPatientList.setVisible(self.patients.isEmpty, animated: true, onComplete: nil)
        }
    }
}

extension PatientList: UITableViewDelegate, UITableViewDataSource {
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchField.isFirstResponder ? 0 : patients.count
    }
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: patientCellID, for: indexPath) as! PatientTableViewCell
        cell.selectionStyle = .none
        
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let patientCell = cell as? PatientTableViewCell else { return }
        
        let patient = patients[indexPath.row]
        let totalNotifications = patient.notificationReports?.count ?? 0
        
        let name = patient.name ?? ""
        let lastnameFather = patient.lastnameFather ?? ""
        let lastnameMother = patient.lastnameMother ?? ""
        
        patientCell.nameLabel.text = "\(name) \(lastnameFather) \(lastnameMother)".trimmingCharacters(in: .whitespaces)
        patientCell.notificationCounterLabel.text = "\(totalNotifications)"
        patientCell.notificationCounterLabel.isHidden = (totalNotifications == 0)
        
        var innitials = (patient.name?.isEmpty == false) ? String((patient.name?.first)!) : ""
        if patient.lastnameFather?.isEmpty == false {
            innitials += String((patient.lastnameFather?.first)!)
        }
        if patient.lastnameMother?.isEmpty == false {
            innitials += String((patient.lastnameMother?.first)!)
        }
        
        patientCell.avatar.setTitle(innitials, for: .normal)
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let patient = patients[indexPath.row]
        performSegue(withIdentifier: "ViewNotifications", sender: patient)
    }
    
    internal func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    internal func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Borrar", handler: { (action, indexPath) in
            let patient = self.patients[indexPath.row]
            if let idCentinelaPatient = patient.idCentinelaPatient {
                _ = Alamofire.request("\(API)/patients/\(idCentinelaPatient)", method: .delete, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders())
            }
            
            self.patients.remove(at: indexPath.row)
            DataBaseManager.context().delete(patient)
            self.updatePatients()
            if self.patients.isEmpty {
                self.emptyPatientList.setVisible(true, animated: true, onComplete: nil)
            }
        })
        deleteAction.backgroundColor = UIColor.Naat.lightOrange
        
        let editAction = UITableViewRowAction(style: .normal, title: "Editar") { (action, indexPath) in
            let patient = self.patients[indexPath.row]
            self.performSegue(withIdentifier: "AddEditPatient", sender: patient)
        }
        
        editAction.backgroundColor = UIColor.Naat.blue
        
        return [editAction, deleteAction]
    }
}

extension PatientList: UITextFieldDelegate {
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
        patientTable.reloadData()
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
        filterResults()
    }
}
