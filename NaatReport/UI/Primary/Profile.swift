//
//  Profile.swift
//  NaatReport
//
//  Created by System on 11/21/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import MessageUI
import SDWebImage
import UIKit

class Profile: UIViewController {
    @IBOutlet weak var userAvatar: UIButton!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var privacyOption: NextIndicatorUIView!
    @IBOutlet weak var emailOption: NextIndicatorUIView!
    @IBOutlet weak var logoutOption: NextIndicatorUIView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userAvatar.clipsToBounds = true
        userAvatar.layer.cornerRadius = userAvatar.bounds.height / 2.0
        userAvatar.layer.borderColor = UIColor.Naat.orange.cgColor
        userAvatar.layer.borderWidth = 1.0
        
        let centinelaUsers = DataBaseManager.fetchObjects(objectName: "CentinelaUser") as? [CentinelaUser]
        let appUsers = DataBaseManager.fetchObjects(objectName: "ApplicationUser") as? [ApplicationUser]
        
        if let centinelaUser = centinelaUsers?.first {
            var innitials = (centinelaUser.name?.isEmpty == false) ? String((centinelaUser.name?.first)!) : ""
            if centinelaUser.lastnameFather?.isEmpty == false {
                innitials += String((centinelaUser.lastnameFather?.first)!)
            }
            if centinelaUser.lastnameMother?.isEmpty == false {
                innitials += String((centinelaUser.lastnameMother?.first)!)
            }
            
            email.text = centinelaUser.email
            userAvatar.setTitle(innitials, for: .normal)
        }
        else if let appUser = appUsers?.first {
            var innitials = (appUser.name?.isEmpty == false) ? String((appUser.name?.first)!) : ""
            if appUser.lastnameFather?.isEmpty == false {
                innitials += String((appUser.lastnameFather?.first)!)
            }
            if appUser.lastnameMother?.isEmpty == false {
                innitials += String((appUser.lastnameMother?.first)!)
            }
            
            email.text = appUser.email
            userAvatar.setTitle(innitials, for: .normal)
        }
        
        privacyOption.delegate = self
        logoutOption.delegate = self
        emailOption.delegate = self
        
        privacyOption.icon = ""
        logoutOption.icon = ""
        emailOption.icon = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

extension Profile: NextIndicatorUIViewDelegate {
    func nextIndicatorViewSelected(_ nextIndicatorView: NextIndicatorUIView) {
        if nextIndicatorView.isEqual(privacyOption) {
            present(Privacy(), animated: true, completion: nil)
        }
        else if nextIndicatorView.isEqual(logoutOption) {
            UtilitySession.setSessionToken(nil)
            DataBaseManager.clearDataBase()
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
            
            navigationController?.popViewController(animated: true)
        }
        else if nextIndicatorView.isEqual(emailOption) {
            let composer = MFMailComposeViewController()
            
            if MFMailComposeViewController.canSendMail() {
                composer.mailComposeDelegate = self
                composer.setToRecipients(["naatreport@digitalizatxt.com"])
                composer.setSubject("Ayuda")
                composer.setMessageBody("", isHTML: false)
                present(composer, animated: true, completion: nil)
            }
            
            //UIApplication.shared.open(URL(string: "http://digitalizatxt.com/")!, options: [:], completionHandler: nil)
        }
    }
}

extension Profile: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
