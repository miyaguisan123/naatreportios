//
//  DateSelector.swift
//  NaatReport
//
//  Created by System on 11/30/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import JTAppleCalendar
import UIKit

protocol DateSelectorDelegate: class {
    func dateSelected(_ date: Date?)
}

class DateSelector: BaseViewController {
    @IBOutlet weak var currentMonthButton: UIButton!
    @IBOutlet weak var calendarContainer: UIView!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var datePickerHeightConstraint: NSLayoutConstraint!
    
    var currentYear: Int = 2018
    var minDate: Date = Date()
    var maxDate: Date = Date()
    var selectedDate: Date = Date() {
        didSet {
            delegate?.dateSelected(selectedDate)
        }
    }
    
    public weak var delegate: DateSelectorDelegate? = nil
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarContainer.layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        calendarView.calendarDelegate = self
        calendarView.calendarDataSource = self
        calendarView.register(UINib(nibName: "DateSelectorJTAppleCell", bundle: nil), forCellWithReuseIdentifier: "dateCell")
        
        calendarView.visibleDates({ (visibleDates) in
            self.updateMonth(from: visibleDates)
        })
        
        datePickerHeightConstraint.constant = 0.0
        
        let date = Date()
        let calendar = Calendar.current
        currentYear = calendar.component(.year, from: date)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        calendarView.scrollToDate(selectedDate, triggerScrollToDateDelegate: false, animateScroll: false, completionHandler: {
            self.calendarView.selectDates([self.selectedDate])
        })
    }
    
    func updateMonth(from visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first?.date
        currentMonthButton.setTitle(date?.stringInFormat(.monthAndYear), for: .normal)
    }
    
    override func dismissAction() {
        setDatePickerVisible(false)
    }
    
    @IBAction func jumptToNextMonth() {
        calendarView.scrollToSegment(.next)
    }
    
    @IBAction func jumptToPrevious() {
        calendarView.scrollToSegment(.previous)
    }
    
    @IBAction func close() {
        dismiss(animated: true, completion: nil)
        setDatePickerVisible(false)
    }
}

extension DateSelector: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    internal func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        return ConfigurationParameters(startDate: minDate, endDate: maxDate, numberOfRows: 6, calendar: Calendar.current, generateInDates: .forAllMonths, generateOutDates: .tillEndOfGrid, firstDayOfWeek: .sunday)
    }
    
    internal func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let dateSelectorCell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DateSelectorJTAppleCell
        self.calendar(calendar, willDisplay: dateSelectorCell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        dateSelectorCell.layer.borderColor = UIColor.Naat.borderGray.cgColor
        dateSelectorCell.layer.borderWidth = 0.5
        
        return dateSelectorCell
    }
    
    internal func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        guard let dateSelectorCell = cell as? DateSelectorJTAppleCell else { return }
        
        dateSelectorCell.dayLabel.text = cellState.text
        updateCell(dateSelectorCell, cellState)
    }
    
    internal func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let dateSelectorCell = cell as? DateSelectorJTAppleCell else { return }
        
        if date.compare(maxDate) == .orderedDescending {
            calendar.deselectAllDates()
            return
        }
        
        dismissAction()
        updateCell(dateSelectorCell, cellState)
        selectedDate = date
    }
    
    internal func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let dateSelectorCell = cell as? DateSelectorJTAppleCell else {
            return
        }
        
        updateCell(dateSelectorCell, cellState)
    }
    
    internal func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        updateMonth(from: visibleDates)
    }
}

extension DateSelector {
    func updateCell(_ cell: JTAppleCell, _ state: CellState) {
        guard let dateSelectorCell = cell as? DateSelectorJTAppleCell else { return }
        
        if state.isSelected {
            dateSelectorCell.currentDayIndicator.isHidden = true
            dateSelectorCell.selectedIndicator.isHidden = false
            dateSelectorCell.dayLabel.textColor = .white
        }
        else {
            var calendar = Calendar.current
            calendar.timeZone = TimeZone(secondsFromGMT: 0)!
            let datesAreInTheSameDay = calendar.isDate(Date(), equalTo: state.date, toGranularity:.day)
            
            dateSelectorCell.currentDayIndicator.isHidden = (datesAreInTheSameDay == false)
            dateSelectorCell.selectedIndicator.isHidden = true
            
            if datesAreInTheSameDay {
                dateSelectorCell.dayLabel.textColor = .white
            }
            else {
                dateSelectorCell.dayLabel.textColor = (state.dateBelongsTo == .thisMonth) ? UIColor.Naat.textColor : UIColor.Naat.lightGray
            }
        }
    }
}

extension DateSelector: UIPickerViewDelegate, UIPickerViewDataSource {
    @IBAction func showYearPicker() {
        setDatePickerVisible(true)
    }
    
    func setDatePickerVisible(_ visible: Bool) {
        datePickerHeightConstraint.constant = visible ? 162.0 : 0.0
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 110
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\((currentYear - row))"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let desiredYear = currentYear - row
        
        let calendar = Calendar.current
        let month = calendar.component(.month, from: selectedDate)
        let day = calendar.component(.day, from: selectedDate)
        
        var newDate = DateComponents()
        newDate.year = desiredYear
        newDate.month = month
        newDate.day = day
        
        selectedDate = calendar.date(from: newDate)!
        calendarView.scrollToDate(selectedDate, triggerScrollToDateDelegate: false, animateScroll: false, completionHandler: {
            self.calendarView.selectDates([self.selectedDate])
            self.calendarView.visibleDates({ (visibleDates) in
                self.updateMonth(from: visibleDates)
            })
        })
    }
}
