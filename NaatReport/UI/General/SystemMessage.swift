import UIKit

class SystemMessage: UIView {
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    var timer: Timer?
    
    deinit {
        self.destroyTimer()
    }
    
    func destroyTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
        }
        
        self.timer = nil
    }
    
    @discardableResult public init(title: String!, message: String!) {
        super.init(frame: .zero)
        self.loadUI()
        
        titleLabel.text = title
        messageLabel.text = message
        
        titleLabel.frame.size.height = titleLabel.sizeThatFits(CGSize(width: titleLabel.frame.size.width, height: .greatestFiniteMagnitude)).height
        messageLabel.frame.size.height = messageLabel.sizeThatFits(CGSize(width: messageLabel.frame.size.width, height: .greatestFiniteMagnitude)).height
        container.reorder(direction: .vertical, resize: true, ignoreFilters: [], offset: ViewOffset(bottom: 10.0, left: 0.0, right: 0.0, top: 10.0), spacing: 5.0, initialMargin: 8.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadUI() {
        alpha = 0.0
        autoresizesSubviews = true
        
        let xibView = Bundle.main.loadNibNamed("SystemMessage", owner: self, options: nil)?[0] as! UIView
        
        let bounds = UIScreen.main.bounds
        let appWindow = UIApplication.shared.delegate?.window
        frame = CGRect(x: 10, y: 0, width: bounds.size.width - 20.0, height: xibView.frame.size.height)
        xibView.frame = self.bounds
        xibView.layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        xibView.layer.borderColor = UIColor.darkGray.cgColor
        xibView.layer.borderWidth = 1.0
        xibView.clipsToBounds = true
        addSubview(xibView)
        
        appWindow??.addSubview(self)
        show(animated: true)
    }
    
    public func show(animated: Bool) {
        if self.titleLabel.text?.isEmpty == true || self.messageLabel.text?.isEmpty == true {
            removeFromSuperview()
            return
        }
        
        frame.origin.y = -(bounds.height + 10.0)
        isHidden = false
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .allowUserInteraction],
                       animations: {
                        self.alpha = 1.0
                        self.frame.origin.y = 20.0
        }, completion: {(finished) in
            self.timer = Timer.scheduledTimer(timeInterval: 5.0,
                                                     target: self,
                                                     selector: #selector(self.dismissMessage),
                                                     userInfo: nil,
                                                     repeats: false)
        })
    }
    
    @IBAction func dismissMessage() {
        destroyTimer()
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .allowUserInteraction],
                       animations: {
                        self.frame.origin.y = -(self.bounds.height + 10.0)
        }, completion: {(finished) in
            self.removeFromSuperview()
        })
    }
}
