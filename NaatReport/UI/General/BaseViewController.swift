//
//  BaseViewController.swift
//  NaatReport
//
//  Created by Miyagui on 11/23/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Photos
import SDWebImage
import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        UIApplication.shared.statusBarStyle = .lightContent
        view.isUserInteractionEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        view.isUserInteractionEnabled = false
    }
    
    func networkRequestFailed() {
        self.showAlert(title: nil, message: nil)
    }
    
    func showAlert(title: String?, message: String?) {
        var alertTitle = title
        var alertMessage = message
        
        if title == nil || title?.isEmpty == true {
            alertTitle = "Error de conexión"
        }
        
        if alertMessage == nil {
            alertMessage = "Ocurrió un error al procesar su petición!"
        }
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(okButton)
        
        present(alert, animated: true, completion: nil)
    }
    
    func dismissAction() {
        //override
    }
}

extension BaseViewController: UIGestureRecognizerDelegate {
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let touchView = touch.view
        if touchView is UITextField || touchView is UIButton || touchView is UITextField || touchView?.superview is UITableViewCell || (touchView?.frame.size.width == 40.0 && touchView?.frame.size.height == 40.0) {
            return false
        }
        
        return true
    }
    
    @objc func handleTap(_ gesture: UITapGestureRecognizer) {
        if gesture.state == .recognized {
            self.dismissAction()
        }
    }
}

extension BaseViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func addContent(from source: UIImagePickerControllerSourceType, sourceRect: CGRect) {
        if UIImagePickerController.isSourceTypeAvailable(source) == false {
            return
        }
        
        if source == .camera {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (response) in
                if response {
                    self.presentContentAccess(from: source, sourceRect: sourceRect)
                }
            })
        }
        else {
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined {
                PHPhotoLibrary.requestAuthorization({status in
                    if status == .authorized{
                        self.presentContentAccess(from: source, sourceRect: sourceRect)
                    }
                })
            }
            else {
                presentContentAccess(from: source, sourceRect: sourceRect)
            }
        }
    }
    
    func presentContentAccess(from source: UIImagePickerControllerSourceType, sourceRect: CGRect) {
        var imagePicker: UIImagePickerController!
        imagePicker =  UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = source
        if source == .camera {
            imagePicker.cameraCaptureMode = .photo
        }
        
        imagePicker.modalPresentationStyle = .overCurrentContext
        present(imagePicker, animated: true, completion: nil)
        imagePicker.popoverPresentationController?.sourceView = view
        imagePicker.popoverPresentationController?.permittedArrowDirections = .up
        imagePicker.popoverPresentationController?.sourceRect = sourceRect
    }
    
    internal func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.delegate = nil
        picker.dismiss(animated: true, completion: nil)
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.delegate = nil
        picker.dismiss(animated: true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        var name = "Photo.JPG"
        
        if info.keys.contains(UIImagePickerControllerReferenceURL) {
            guard let refURL = info[UIImagePickerControllerReferenceURL] as? URL else {
                gotImageFromContent(image!, name: name)
                return
            }
            
            
            let newAssets = PHAsset.fetchAssets(withALAssetURLs: [refURL], options: nil)
            let asset = newAssets.lastObject
            let options = PHImageRequestOptions()
            options.isSynchronous = false
            
            if asset != nil {
                PHImageManager.default().requestImageData(for: asset!, options: options, resultHandler: { (data: Data?, identificador: String?, orientaciomImage: UIImageOrientation, info: [AnyHashable: Any]?) -> Void in
                    if data != nil {
                        DispatchQueue.main.async(execute: { () -> Void in
                            if (info?.keys.contains("PHImageFileURLKey"))! {
                                let urlKey = info?["PHImageFileURLKey"] as? NSURL
                                if urlKey != nil && urlKey?.absoluteString != nil {
                                    let string:NSString = NSString(string:urlKey!.absoluteString!)
                                    name = string.lastPathComponent
                                }
                            }
                            
                            self.gotImageFromContent(image!, name: name)
                        })
                    }
                })
            }
            else {
                gotImageFromContent(image!, name: name)
            }
        }
        else {
            gotImageFromContent(image!, name: name)
        }
    }
    
    @objc func gotImageFromContent(_ image: UIImage, name: String) {
        //override on specific subclasses
    }
}

extension BaseViewController {
    @objc func keyboardWillShow(_ notification: Notification?) {
        let keyboardSize = notification?.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.size.height
        let keyboardAnimationDuration = notification?.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let duration = TimeInterval(keyboardAnimationDuration.floatValue)
        
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.view.frame.size.height = UIScreen.main.bounds.height - keyboardHeight
                        self.keyboardWillShow(height: keyboardHeight, animationDuration: duration)
        }, completion: nil)
    }
    
    @objc func keyboardWillHide(_ notification: Notification?) {
        let keyboardAnimationDuration = notification?.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        let duration = TimeInterval(keyboardAnimationDuration.floatValue)
        
        UIView.animate(withDuration: duration,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.view.frame.size.height = UIScreen.main.bounds.height
                        self.keyboardWillHide(animationDuration: duration)
        }, completion: nil)
    }
    
    @objc public func keyboardWillShow(height: CGFloat, animationDuration: TimeInterval) {
        // to be override
    }
    
    @objc public func keyboardWillHide(animationDuration: TimeInterval) {
        // to be override
    }
}

extension BaseViewController {
    func reviewTokenConflict(_ jsonResponse: String?) -> Bool {
        if let jsonDictionary = jsonResponse?.json() as? [AnyHashable:Any], let conflictError = jsonDictionary["error"] as? String, conflictError == "invalid_session_token", let conflictHttpStatus = jsonDictionary["severity"] as? String, conflictHttpStatus == "CONFLICT" {
            destroyUserSession()
            return true
        }
        
        return false
    }
    
    func destroyUserSession() {
        UtilitySession.setSessionToken(nil)
        DataBaseManager.clearDataBase()
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        
        navigationController?.popToRootViewController(animated: true)
    }
}
