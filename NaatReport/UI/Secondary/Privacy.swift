//
//  Privacy.swift
//  NaatReport
//
//  Created by Miyagui on 12/26/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

class Privacy: UIViewController {
    var idCentinelaPatient: String? = nil
    
    deinit {
        idCentinelaPatient = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ScanCode" {
            if let ep = segue.destination as? ScanCode {
                ep.idCentinelaPatient = self.idCentinelaPatient
            }
        }
    }
}
