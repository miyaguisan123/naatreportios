//
//  AddNotificationExtras.swift
//  NaatReport
//
//  Created by Miyagui on 4/2/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import UIKit

protocol AddNotificationExtrasDelegate: class {
    func extrasDidSelect(severity: String, gravity: String)
}

class AddNotificationExtras: UIViewController {
    @IBOutlet weak var severityField: CustomUITextField!
    @IBOutlet weak var gravityField: CustomUITextField!
    @IBOutlet weak var picker: UIPickerView!
    
    weak var delegate: AddNotificationExtrasDelegate? = nil
    
    let options = [0:["Leve", "Moderado", "Severo"], 1:["No Grave", "Grave"]]
    var pickSection = 0 {
        didSet {
            picker.reloadAllComponents()
            
            if pickSection == 0 {
                picker.selectRow(severity, inComponent: 0, animated: false)
            }
            else {
                picker.selectRow(gravity, inComponent: 0, animated: false)
            }
            
            picker.setVisible(true, animated: true, onComplete: nil)
        }
    }
    var severity = 0
    var gravity = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func selectPickSection(_ sender: UIButton) {
        pickSection = sender.tag
    }
    
    @IBAction func acceptChanges() {
        let severityOptions = options[0]
        let gravityOptions = options[1]
        let selectedSeverity = severityOptions![severity]
        let selectedGravity = gravityOptions![gravity]
        
        delegate?.extrasDidSelect(severity: selectedSeverity, gravity: selectedGravity)
        dismiss(animated: true, completion: nil)
    }
}

extension AddNotificationExtras: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let componentOptions = options[pickSection]
        return componentOptions?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let componentOptions = options[pickSection]
        return componentOptions![row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let componentOptions = options[pickSection]
        
        if pickSection == 0 {
            severity = row
            severityField.text = componentOptions?[row]
        }
        else {
            gravity = row
            gravityField.text = componentOptions?[row]
        }
    }
}
