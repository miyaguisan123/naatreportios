//
//  RegisterMedicine.swift
//  NaatReport
//
//  Created by Miyagui on 2/11/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

protocol RegisterMedicineDelegate: class {
    func medicineRegisterDidCancel()
    func medicineDidRegister(_ medicine: CentinelaMedicine, lot: String, expirationDate: Date, serialNum: String, origin: String, notes: String)
}

class RegisterMedicine: BaseViewController {
    @IBOutlet weak var registerProductContainer: UIScrollView!
    
    @IBOutlet weak var registerBarCodeField: CustomUITextField!
    @IBOutlet weak var registerNameField: CustomUITextField!
    @IBOutlet weak var registerLaboratoryField: CustomUITextField!
    @IBOutlet weak var registerActiveIngredientField: CustomUITextField!
    @IBOutlet weak var registerLotField: CustomUITextField!
    @IBOutlet weak var registerSerialNumber: CustomUITextField!
    
    @IBOutlet weak var registerPresentation: CustomUITextField!
    @IBOutlet weak var registerFormula: CustomUITextField!
    
    @IBOutlet weak var registerExpirationMonthField: CustomUITextField!
    @IBOutlet weak var registerExpirationYearField: CustomUITextField!
    
    @IBOutlet weak var registerActivityIndicator: CircularActivityIndicator!
    
    @IBOutlet weak var closeButton: UIButton!
    
    var medicineBarCode: String? = nil
    var medicineLot: String? = nil
    var medicineSerialNumber: String? = nil
    var medicineExpirationMonth: String? = nil
    var medicineExpirationYear: String? = nil
    
    weak var delegate: RegisterMedicineDelegate? = nil
    
    deinit {
        medicineBarCode = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerProductContainer.layer.borderColor = UIColor.Naat.orange.cgColor
        registerProductContainer.layer.borderWidth = 2.0
        registerProductContainer.layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        registerProductContainer.translatesAutoresizingMaskIntoConstraints = false
        
        registerBarCodeField.text = medicineBarCode
        registerLotField.text = medicineLot
        registerSerialNumber.text = medicineSerialNumber
        registerExpirationMonthField.text = medicineExpirationMonth
        registerExpirationYearField.text = medicineExpirationYear
        
        closeButton.layer.borderColor = UIColor.Naat.orange.cgColor
        closeButton.layer.borderWidth = 2.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if medicineBarCode?.isEmpty == true {
            registerBarCodeField.isUserInteractionEnabled = true
            registerBarCodeField.becomeFirstResponder()
        }
        else {
            registerNameField.becomeFirstResponder()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func formIsValid() -> Bool {
        var formIsValid = true
        
        for view in registerProductContainer.subviews {
            guard let field = view as? CustomUITextField else { continue }
            field.resignFirstResponder()
            if field.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
                field.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                formIsValid = false
            }
        }
        
        if registerExpirationMonthField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
            registerExpirationMonthField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
        }
        
        if registerExpirationYearField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
            registerExpirationYearField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
        }
        
        return formIsValid
    }
    
    @IBAction func registerProduct() {
        view.endEditing(true)
        
        guard formIsValid() else { return }
        
        guard let month = registerExpirationMonthField.text, month.trimmingCharacters(in: .whitespaces).isEmpty == false, let year = registerExpirationYearField.text, year.trimmingCharacters(in: .whitespaces).isEmpty == false else {
            registerExpirationMonthField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.white.cgColor, showDefaultBorder: false, animated: true)
            registerExpirationYearField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.white.cgColor, showDefaultBorder: false, animated: true)
            SystemMessage(title: "Fecha de caducidad inválida", message: "")
            return
        }
        
        let userCalendar = Calendar.current
        var expirationDateComponents = DateComponents()
        expirationDateComponents.year = Int(year)
        expirationDateComponents.month = Int(month)
        expirationDateComponents.day = 1
        
        let expirationDate = userCalendar.date(from: expirationDateComponents)
        if expirationDate == nil {
            registerExpirationMonthField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.white.cgColor, showDefaultBorder: false, animated: true)
            registerExpirationYearField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.white.cgColor, showDefaultBorder: false, animated: true)
            SystemMessage(title: "Fecha de caducidad inválida", message: "")
            return
        }
        
        registerActivityIndicator.startInderminateAnimation()
        registerProductContainer.setVisible(false, animated: true, onComplete: nil)
        
        var params = [String:Any]()
        params["barcode"] = self.registerBarCodeField.text
        params["name"] = self.registerNameField.text
        params["laboratory"] = self.registerLaboratoryField.text
        params["active_ingredient"] = self.registerActiveIngredientField.text
        params["exporation_date"] = expirationDate?.stringInFormat(.slashedNumericStandard)
        params["serial_number"] = self.registerSerialNumber.text
        params["formula"] = self.registerFormula.text
        params["presentation"] = self.registerPresentation.text
        
        let lot = self.registerLotField.text ?? ""
        let serialNum = self.registerSerialNumber.text ?? ""
        let origin = "NAAT REPORT"
        let notes = "Created from App"
        
        Alamofire.request("\(API)/addMedicine", method: .post, parameters: params, encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseString { (response) in
            switch response.result {
            case .success (let string):
                if self.reviewTokenConflict(string) { return }
                
                guard let medicineInfo = string.json() as? [AnyHashable:Any], medicineInfo.keys.contains("id") else {
                    SystemMessage(title: "Error al registrar el producto", message: "Ocurrió un error al registrar el producto.")
                    return
                }
                
                if let medicine = DataBaseManager.updateCentinelaMedicine(medicineInfo, notification: nil) {
                    self.delegate?.medicineDidRegister(medicine, lot: lot, expirationDate: expirationDate!, serialNum: serialNum, origin: origin, notes: notes)
                }
                else {
                    SystemMessage(title: "Error al registrar el producto", message: "Ocurrió un error al registrar el producto.")
                }
            case .failure(let error):
                print(error)
                break
            }
            
            self.close()
        }
    }
    
    @IBAction func close() {
        delegate?.medicineRegisterDidCancel()
        dismiss(animated: true, completion: nil)
    }
}

extension RegisterMedicine {
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            registerNameField.text = "NEXT"
            registerLaboratoryField.text = "Genomma Lab"
            registerLotField.text = "017Y134"
            registerExpirationMonthField.text = "05"
            registerExpirationYearField.text = "2019"
            registerActiveIngredientField.text = "Paracetamol"
            registerSerialNumber.text = "Y97H24NJ"
        }
    }
}

extension RegisterMedicine: UITextFieldDelegate {
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(registerLotField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 25
        }
        
        return true
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == false {
            if textField.isEqual(registerBarCodeField) {
                registerNameField.becomeFirstResponder()
            }
            else if textField.isEqual(registerNameField) {
                registerLaboratoryField.becomeFirstResponder()
            }
            else if textField.isEqual(registerLaboratoryField) {
                registerActiveIngredientField.becomeFirstResponder()
            }
            else if textField.isEqual(registerActiveIngredientField) {
                registerLotField.becomeFirstResponder()
            }
            else if textField.isEqual(registerLotField) {
                registerSerialNumber.becomeFirstResponder()
            }
            else if textField.isEqual(registerSerialNumber) {
                registerPresentation.becomeFirstResponder()
            }
            else if textField.isEqual(registerPresentation) {
                registerFormula.becomeFirstResponder()
            }
            else if textField.isEqual(registerFormula) {
                registerExpirationMonthField.becomeFirstResponder()
            }
            else if textField.isEqual(registerExpirationMonthField) {
                registerExpirationYearField.becomeFirstResponder()
            }
            else {
                textField.resignFirstResponder()
            }
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}
