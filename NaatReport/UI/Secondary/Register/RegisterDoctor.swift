//
//  RegisterDoctor.swift
//  NaatReport
//
//  Created by System on 11/30/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class RegisterDoctor: BaseViewController {
    @IBOutlet weak var top: UIView!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var createAccountButton: UIButton!
    
    @IBOutlet weak var nameField: CustomUITextField!
    @IBOutlet weak var surnameFather: CustomUITextField!
    @IBOutlet weak var surnameMother: CustomUITextField!
    
    @IBOutlet weak var usernameField: CustomUITextField!
    @IBOutlet weak var passwordField: CustomUITextField!
    @IBOutlet weak var confirmPasswordField: CustomUITextField!
    @IBOutlet weak var emailField: CustomUITextField!
    @IBOutlet weak var phoneField: CustomUITextField!
    @IBOutlet weak var dateField: CustomUITextField!
    @IBOutlet weak var genderFButton: UIButton!
    @IBOutlet weak var genderMButton: UIButton!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: CircularActivityIndicator!
    
    var initialOffset: CGFloat = 0.0
    
    private var birthDate: Date? = nil {
        didSet {
            if birthDate != nil {
                dateField.text = birthDate?.stringInFormat(.slashedStandard)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        birthDate = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentScroll.contentSize.height = (contentScroll.subviews.last?.bottom)! + 30.0
        initialOffset = contentScroll.contentOffset.y
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nameField.becomeFirstResponder()
    }
    
    override func dismissAction() {
        view.endEditing(true)
    }
    
    @IBAction func requestBirthDate() {
        view.endEditing(true)
        
        let userCalendar = Calendar.current
        var minimumBirthDate = DateComponents()
        minimumBirthDate.year = 1920
        minimumBirthDate.month = 1
        minimumBirthDate.day = 1
        
        let ds = DateSelector()
        ds.modalPresentationStyle = .overCurrentContext
        ds.minDate = userCalendar.date(from: minimumBirthDate)!
        ds.selectedDate = birthDate ?? Date()
        ds.delegate = self
        
        present(ds, animated: false, completion: nil)
    }
    
    @IBAction func setGender(_ sender: UIButton) {
        sender.isSelected = true
        
        if sender.isEqual(genderFButton) {
            genderMButton.isSelected = false
        }
        else {
            genderFButton.isSelected = false
        }
    }
    
    @IBAction func createAccount() {
        guard validateFields() else { return }
        
        contentScroll.endEditing(true)
        contentScroll.setVisible(false, animated: true, onComplete: nil)
        var params = [String:Any]()
        params["name"] = nameField.text
        params["father_lastname"] = surnameFather.text
        params["mother_lastname"] = surnameMother.text
        
        params["gender"] = genderMButton.isSelected
        params["birth_date"] = birthDate?.stringInFormat(.slashedNumericStandard)
        params["email"] = emailField.text
        params["phone"] = phoneField.text
        
        params["username"] = usernameField.text?.filter({!" \n\t\r".contains($0)})
        params["password"] = passwordField.text
        params["password_confirmation"] = confirmPasswordField.text
        params["type"] = "doctor"
        
        loadingIndicator.startInderminateAnimation()
        loadingView.setVisible(false, animated: true, onComplete: nil)
        
        Alamofire.request("\(API)/register", method: .post, parameters: params, encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseJSON { (response) in
            self.loadingIndicator.stopInderminateAnimation()
            self.loadingView.setVisible(false, animated: true, onComplete: nil)
            
            switch response.result {
            case .success(let result):
                guard let info = result as? [AnyHashable:Any] else { return }
                
                if let error = info["error"] as? String {
                    SystemMessage(title: "Error", message: "El servidor produjo el siguiente error: \(error)")
                }
                else {
                    self.close()
                    SystemMessage(title: "Registrado", message: "Se le ha enviado un correo de confirmación. Verifique su cuenta.")
                }
                break
            case .failure(let error):
                print(error)
                self.networkRequestFailed()
                self.contentScroll.setVisible(true, animated: true, onComplete: nil)
                break
            }
        }
    }
    
    @IBAction func close() {
        view.endEditing(true)
        navigationController?.popToRootViewController(animated: true)
    }
}

extension RegisterDoctor {
    func validateFields() -> Bool {
        var formIsValid = true
        
        for view in contentScroll.subviews {
            guard let field = view as? CustomUITextField else { continue }
            
            if field.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
                field.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                formIsValid = false
            }
        }
        
        if (confirmPasswordField.text != passwordField.text) {
            passwordField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            confirmPasswordField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
        }
        
        if genderFButton.isSelected == false && genderMButton.isSelected == false {
            genderFButton.setTitleColor(UIColor.red, for: .normal)
            genderMButton.setTitleColor(UIColor.red, for: .normal)
            formIsValid = false
        }
        
        return formIsValid
    }
}

extension RegisterDoctor: UITextFieldDelegate {
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
    
    internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == false {
            if textField.isEqual(nameField) {
                surnameFather.becomeFirstResponder()
            }
            else if textField.isEqual(surnameFather) {
                surnameMother.becomeFirstResponder()
            }
            else if textField.isEqual(surnameMother) {
                textField.resignFirstResponder()
            }
            else if textField.isEqual(usernameField) {
                let currentUsername = usernameField.text?.filter({!" \n\t\r".contains($0)})
                usernameField.text = currentUsername
                passwordField.becomeFirstResponder()
            }
            else if textField.isEqual(passwordField) {
                confirmPasswordField.becomeFirstResponder()
            }
            else if textField.isEqual(confirmPasswordField) {
                emailField.becomeFirstResponder()
            }
            else if textField.isEqual(emailField) {
                phoneField.becomeFirstResponder()
            }
            else if textField.isEqual(phoneField) {
                resignFirstResponder()
                requestBirthDate()
            }
            else {
                textField.resignFirstResponder()
            }
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}

extension RegisterDoctor: DateSelectorDelegate {
    internal func dateSelected(_ date: Date?) {
        birthDate = date
        
        self.dateField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}
