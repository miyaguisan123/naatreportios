//
//  Register.swift
//  NaatReport
//
//  Created by System on 11/30/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class Register: BaseViewController {
    @IBOutlet weak var top: UIView!
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var createAccountButton: UIButton!
    
    @IBOutlet weak var nameField: CustomUITextField!
    @IBOutlet weak var surnameFather: CustomUITextField!
    @IBOutlet weak var surnameMother: CustomUITextField!
    @IBOutlet weak var rfc: CustomUITextField!
    
    @IBOutlet weak var usernameField: CustomUITextField!
    @IBOutlet weak var passwordField: CustomUITextField!
    @IBOutlet weak var confirmPasswordField: CustomUITextField!
    @IBOutlet weak var emailField: CustomUITextField!
    @IBOutlet weak var dateField: CustomUITextField!
    @IBOutlet weak var genderFButton: UIButton!
    @IBOutlet weak var genderMButton: UIButton!
    @IBOutlet weak var heightField: CustomUITextField!
    @IBOutlet weak var phoneField: CustomUITextField!
    @IBOutlet weak var weightField: CustomUITextField!
    @IBOutlet weak var postalCode: CustomUITextField!
    @IBOutlet weak var locationField: CustomUITextField!
    @IBOutlet weak var neighborhood: CustomUITextField!
    
    @IBOutlet weak var neighborhoodPicker: UIPickerView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: CircularActivityIndicator!
    
    var idHood = 0
    var idCity = 0
    var idState = 0
    
    var initialOffset: CGFloat = 0.0
    lazy var neighborhoods = [(Int,String)]()
    
    private var birthDate: Date? = nil {
        didSet {
            if birthDate != nil {
                dateField.text = birthDate?.stringInFormat(.slashedStandard)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        neighborhoods.removeAll()
        birthDate = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentScroll.contentSize.height = (contentScroll.subviews.last?.bottom)! + 30.0
        
        initialOffset = contentScroll.contentOffset.y
        neighborhoodPicker.frame.origin.y = view.bounds.height
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nameField.becomeFirstResponder()
    }
    
    override func dismissAction() {
        view.endEditing(true)
        dismissPicker()
    }
    
    @IBAction func requestBirthDate() {
        view.endEditing(true)
        
        let userCalendar = Calendar.current
        var minimumBirthDate = DateComponents()
        minimumBirthDate.year = 1920
        minimumBirthDate.month = 1
        minimumBirthDate.day = 1
        
        let ds = DateSelector()
        ds.modalPresentationStyle = .overCurrentContext
        ds.minDate = userCalendar.date(from: minimumBirthDate)!
        ds.selectedDate = birthDate ?? Date()
        ds.delegate = self
        
        present(ds, animated: false, completion: nil)
    }
    
    @IBAction func setGender(_ sender: UIButton) {
        sender.isSelected = true
        
        if sender.isEqual(genderFButton) {
            genderMButton.isSelected = false
        }
        else {
            genderFButton.isSelected = false
        }
    }
    
    @IBAction func createAccount() {
        guard validateFields() else { return }
        
        contentScroll.endEditing(true)
        contentScroll.setVisible(false, animated: true, onComplete: nil)
        
        var params = [String:Any]()
        params["name"] = nameField.text
        params["father_lastname"] = surnameFather.text
        params["mother_lastname"] = surnameMother.text
        params["rfc"] = rfc.text?.uppercased()
        
        params["gender"] = genderMButton.isSelected
        params["birth_date"] = birthDate?.stringInFormat(.slashedNumericStandard)
        params["email"] = emailField.text
        params["phone"] = phoneField.text
        
        params["location"] = idHood
        params["city"] = idCity
        params["state"] = idState
        
        params["username"] = usernameField.text?.filter({!" \n\t\r".contains($0)})
        params["password"] = passwordField.text
        params["password_confirmation"] = confirmPasswordField.text
        params["weight"] = weightField.text
        params["height"] = heightField.text
        params["type"] = "patient"
        
        loadingIndicator.startInderminateAnimation()
        loadingView.setVisible(false, animated: true, onComplete: nil)
        
        Alamofire.request("\(API)/register", method: .post, parameters: params, encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseJSON { (response) in
            self.loadingIndicator.stopInderminateAnimation()
            self.loadingView.setVisible(false, animated: true, onComplete: nil)
            
            switch response.result {
            case .success(let result):
                guard let info = result as? [AnyHashable:Any] else { return }
                
                if let error = info["error"] as? String {
                    SystemMessage(title: "Error", message: "El servidor produjo el siguiente error: \(error)")
                }
                else {
                    self.close()
                    SystemMessage(title: "Registrado", message: "Se le ha enviado un correo de confirmación. Verifique su cuenta.")
                }
                break
            case .failure(let error):
                print(error)
                self.networkRequestFailed()
                self.contentScroll.setVisible(true, animated: true, onComplete: nil)
                break
            }
        }
    }
    
    @IBAction func selectNeighborhood() {
        guard neighborhoodPicker.top == view.frame.size.height, neighborhoods.isEmpty == false else { return }
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.neighborhoodPicker.frame.origin.y = self.view.frame.size.height - self.neighborhoodPicker.frame.size.height
                        self.contentScroll.frame.size.height = self.neighborhoodPicker.top
                        self.contentScroll.contentOffset.y += self.neighborhoodPicker.bounds.height
        }, completion: nil)
    }
    
    @IBAction func close() {
        view.endEditing(true)
        navigationController?.popToRootViewController(animated: true)
    }
    
    func dismissPicker() {
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .allowAnimatedContent, .curveEaseInOut],
                       animations: {
                        self.neighborhoodPicker.frame.origin.y = self.view.frame.size.height
                        self.contentScroll.frame.size.height = self.neighborhoodPicker.top
        }, completion: nil)
    }
}

extension Register {
    func validateFields() -> Bool {
        var formIsValid = true
        
        for view in contentScroll.subviews {
            guard let field = view as? CustomUITextField else { continue }
            
            if field.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
                field.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                formIsValid = false
            }
        }
        
        if (confirmPasswordField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0) < 6 {
            passwordField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            
            showAlert(title: "Contraseña Inválida", message: "La contraseña no puede tener menos de 5 caracteres.")
        }
        else if (confirmPasswordField.text != passwordField.text) {
            passwordField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            confirmPasswordField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
            
            showAlert(title: "Contraseña Inválida", message: "La contraseña y la confirmación no coinciden.")
        }
        else if (rfc.text?.isValidRFC() == false) {
            rfc.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            formIsValid = false
            
            showAlert(title: "RFC Inválido", message: "El RFC que ingresó no es válido.")
        }
        
        if genderFButton.isSelected == false && genderMButton.isSelected == false {
            genderFButton.setTitleColor(UIColor.red, for: .normal)
            genderMButton.setTitleColor(UIColor.red, for: .normal)
            formIsValid = false
        }
        
        return formIsValid
    }
    
    func requestLocationData() {
        let zipCode = postalCode.text
        if zipCode?.trimmingCharacters(in: .whitespaces).isEmpty == false {
            _ = Alamofire.request("\(API)/location?zip=\(zipCode!)", method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
                switch response.result {
                case .success(let string):
                    guard let locationData = string.json() as? [AnyHashable:Any] else { return }
                    
                    self.neighborhoods.removeAll()
                    if let n = locationData["neighborhoods"] as? [[AnyHashable:Any]] {
                        for entry in n {
                            let hoodName = entry["name"] as? String ?? ""
                            let hoodID = entry["id"] as? Int ?? 0
                            
                            self.neighborhoods.append((hoodID, hoodName))
                        }
                        
                        self.neighborhood.text = self.neighborhoods.first?.1
                        self.idHood = self.neighborhoods.first?.0 ?? 0
                    }
                    
                    var cityName = ""
                    var stateName = ""
                    if let city = locationData["city"] as? [AnyHashable:Any] {
                        cityName = city["name"] as! String
                        self.idCity = city["id"] as! Int
                    }
                    
                    if let state = locationData["state"] as? [AnyHashable:Any] {
                        stateName = state["name"] as! String
                        self.idState = state["id"] as! Int
                    }
                    
                    self.locationField.text = "\(cityName), \(stateName)"
                    self.locationField.set(mode: .normal, defaultBackgroundColor: UIColor("#DDDDDD"), defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                    self.locationField.textColor = UIColor("#707070")
                    
                    break
                case .failure:
                    break
                }
                
                self.neighborhoodPicker.reloadAllComponents()
            })
        }
    }
}

extension Register: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(postalCode) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 5
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == false {
            if textField.isEqual(nameField) {
                surnameFather.becomeFirstResponder()
            }
            else if textField.isEqual(surnameFather) {
                surnameMother.becomeFirstResponder()
            }
            else if textField.isEqual(surnameMother) {
                rfc.becomeFirstResponder()
            }
            else if textField.isEqual(rfc) {
                textField.resignFirstResponder()
            }
            else if textField.isEqual(usernameField) {
                let currentUsername = usernameField.text?.filter({!" \n\t\r".contains($0)})
                usernameField.text = currentUsername
                passwordField.becomeFirstResponder()
            }
            else if textField.isEqual(passwordField) {
                confirmPasswordField.becomeFirstResponder()
            }
            else if textField.isEqual(confirmPasswordField) {
                emailField.becomeFirstResponder()
            }
            else if textField.isEqual(emailField) {
                phoneField.becomeFirstResponder()
            }
            else if textField.isEqual(phoneField) {
                textField.resignFirstResponder()
            }
            else if textField.isEqual(heightField) {
                weightField.becomeFirstResponder()
            }
            else if textField.isEqual(weightField) {
                resignFirstResponder()
                requestBirthDate()
            }
            else {
                textField.resignFirstResponder()
            }
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
        if textField.isEqual(postalCode) {
            requestLocationData()
        }
        
        if textField.isEqual(rfc) {
            textField.text = textField.text?.uppercased()
        }
    }
}

extension Register: DateSelectorDelegate {
    internal func dateSelected(_ date: Date?) {
        birthDate = date
        
        self.dateField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}

extension Register: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return neighborhoods.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.neighborhoods[row].1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        idHood = neighborhoods[row].0
        neighborhood.text = neighborhoods[row].1
        
        neighborhood.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}
