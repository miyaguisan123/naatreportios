//
//  RegisterTypeSelection.swift
//  NaatReport
//
//  Created by Miyagui on 12/6/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

class RegisterTypeSelection: UIViewController {
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let seen = UtilitySession.getSessionVar(key: "SEEN_PRIVACY_MESSAGE") as? String {
            if seen == "NO" {
                present(Privacy(), animated: true, completion: nil)
                UtilitySession.setSessionVar(key: "SEEN_PRIVACY_MESSAGE", value: "YES")
            }
        }
    }
    
    @IBAction func close() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerAsPatient() {
        let register = Register()
        navigationController?.pushViewController(register, animated: true)
    }
    
    @IBAction func registerAsDoctor() {
        let register = RegisterDoctor()
        navigationController?.pushViewController(register, animated: true)
    }
}
