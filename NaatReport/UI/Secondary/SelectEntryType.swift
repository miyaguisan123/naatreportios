//
//  SelectEntryType.swift
//  NaatReport
//
//  Created by Miyagui on 2/21/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class SelectEntryType: BaseViewController {
    @IBOutlet weak var contentScroll: UIScrollView!
    @IBOutlet weak var barcodeField: CustomUITextField!
    
    @IBOutlet weak var dismissContainer: UIView!
    
    @IBOutlet weak var scanRequestContainer: UIView!
    @IBOutlet weak var scanResultLabel: UILabel!
    @IBOutlet weak var productInfoContainer: UIView!
    @IBOutlet weak var lotField: CustomUITextField!
    @IBOutlet weak var serialNumberField: CustomUITextField!
    @IBOutlet weak var expirationMonthField: CustomUITextField!
    @IBOutlet weak var expirationYearField: CustomUITextField!
    @IBOutlet weak var continueButton: NaatButton!
    
    @IBOutlet weak var activityIndicator: CircularActivityIndicator!
    
    var idCentinelaPatient: String? = nil
    var idCentinelaMedicine: String? = nil
    var idCentinelaProduct: String? = nil
    
    deinit {
        idCentinelaPatient = nil
        idCentinelaMedicine = nil
        idCentinelaProduct = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentScroll.translatesAutoresizingMaskIntoConstraints = false
        barcodeField.layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        barcodeField.layer.borderColor = UIColor.Naat.borderGray.cgColor
        barcodeField.layer.borderWidth = 1.0
        
        scanRequestContainer.layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        scanRequestContainer.layer.borderColor = UIColor.Naat.orange.cgColor
        scanRequestContainer.layer.borderWidth = 2.0
        
        scanRequestContainer.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func dismissAction() {
        barcodeField.resignFirstResponder()
    }
    
    func requestFailed() {
        dismissResult()
        
    }
    
    @IBAction func dismissResult() {
        barcodeField.text = ""
        expirationMonthField.text = ""
        expirationYearField.text = ""
        
        view.endEditing(true)
        
        scanRequestContainer.setVisible(false, animated: true, onComplete: nil)
        dismissContainer.setVisible(false, animated: true, onComplete: nil)
        contentScroll.setVisible(true, animated: true, onComplete: nil)
        contentScroll.isUserInteractionEnabled = true
        activityIndicator.stopInderminateAnimation()
    }
    
    @IBAction func close() {
        view.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func scanCode() {
        performSegue(withIdentifier: "ScanCode", sender: nil)
    }
    
    @IBAction func tryToFindProduct() {
        barcodeField.resignFirstResponder()
        
        if let barcode = barcodeField.text?.trimmingCharacters(in: .whitespaces), barcode.isEmpty == false {
            contentScroll.isUserInteractionEnabled = false
            activityIndicator.startInderminateAnimation()
            
            dismissContainer.setVisible(true, animated: true, onComplete: nil)
            contentScroll.setVisible(false, animated: true, onComplete: {
                Alamofire.request("\(API)/medicines/\(barcode)?type=barcode", method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
                    self.activityIndicator.stopInderminateAnimation()
                    switch response.result {
                    case .success (let string):
                        if self.reviewTokenConflict(string) { return }
                        
                        guard let medicineInfo = string.json() as? [AnyHashable:Any], medicineInfo.keys.contains("id"), let type = medicineInfo["type"] as? String else {
                            self.requestFailed()
                            SystemMessage.init(title: "Sin resultados", message: "No se encontraron productos o medicamentos con el código de barras especificado.")
                            return
                        }
                        
                        if type == "product", let product = DataBaseManager.updateCentinelaProduct(medicineInfo, notification: nil) {
                            self.centinelaProductFound(product)
                        }
                        else if type == "medicine", let medicine = DataBaseManager.updateCentinelaMedicine(medicineInfo, notification: nil) {
                            self.centinelaMedicineFound(medicine)
                        }
                        
                        break
                    case .failure:
                        self.networkRequestFailed()
                        self.requestFailed()
                        break
                    }
                })
            })
        }
        else {
            barcodeField.becomeFirstResponder()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewReport = segue.destination as? ViewReport, let firstFormInReport = sender as? FormInReport {
            viewReport.idFormInReport = firstFormInReport.idFormInReport
        }
        else if let scanCode = segue.destination as? ScanCode {
            scanCode.idCentinelaPatient = idCentinelaPatient
        }
        else if let pickForm = segue.destination as? FormPicker, let report = sender as? CentinelaReport {
            pickForm.idCentinelaReport = report.idCentinelaReport
        }
    }
}

extension SelectEntryType {
    @IBAction func createReport() {
        var formIsValid = true
        
        for view in scanRequestContainer.subviews {
            guard let field = view as? CustomUITextField else { continue }
            field.resignFirstResponder()
            if field.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
                field.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                formIsValid = false
            }
        }
        
        if formIsValid == false {
            return
        }
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.scanRequestContainer.alpha = 0.0
        }) { (finish) in
            let year = self.expirationYearField.text ?? "0"
            let month = self.expirationMonthField.text ?? "0"
            
            let userCalendar = Calendar.current
            var expirationDateComponents = DateComponents()
            expirationDateComponents.year = Int(year)
            expirationDateComponents.month = Int(month)
            expirationDateComponents.day = 1
            
            if let expirationDate = userCalendar.date(from: expirationDateComponents) {
                self.addNotification(expirationDate: expirationDate, lot: self.lotField.text ?? "", serialNum: self.serialNumberField.text ?? "")
            }
            else {
                self.expirationYearField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                self.expirationMonthField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            }
        }
    }
    
    func addNotification(expirationDate: Date, lot: String, serialNum: String) {
        lotField.resignFirstResponder()
        expirationYearField.resignFirstResponder()
        expirationMonthField.resignFirstResponder()
        
        var params = [String:Any]()
        
        params["idCentinelaPatient"] = self.idCentinelaPatient ?? "0"
        params["idCentinelaMedicine"] = self.idCentinelaMedicine ?? "0"
        params["idCentinelaProduct"] = self.idCentinelaProduct ?? "0"
        params["expiration_date"] = expirationDate.stringInFormat(.slashedNumericStandard)
        params["lot"] = lot
        params["serial_num"] = serialNum
        params["notes"] = "Registered via APP"
        params["origin"] = "NAAT REPORT"
        
        Alamofire.request("\(API)/addNotification", method: .post, parameters: params, encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
            switch response.result {
            case .success (let string):
                if self.reviewTokenConflict(string) { return }
                
                guard let notificationInfo = string.json() as? [AnyHashable:Any] else {
                    self.requestFailed()
                    return
                }
                
                if let notification = DataBaseManager.updateNotificationReport(notificationInfo) {
                    let centinelaUsers = DataBaseManager.fetchObjects(objectName: "CentinelaUser") as? [CentinelaUser]
                    let report = notification.centinelaReport
                    
                    if centinelaUsers?.first != nil {
                        self.performSegue(withIdentifier: "PickForm", sender: report)
                    }
                    else {
                        if let firstForm = report?.forms?.allObjects.first as? FormInReport {
                            self.performSegue(withIdentifier: "ViewReport", sender: firstForm)
                        }
                    }
                }
                
                break
            case .failure:
                self.requestFailed()
                break
            }
        })
    }
    
    func centinelaProductFound(_ product: CentinelaProduct) {
        idCentinelaProduct = product.idCentinelaProduct
        
        let pd = NSMutableAttributedString()
        pd.attributedFont(text: "Nombre: ", size: 14.0, bold: true)
        pd.attributedFont(text: product.name ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Código de Barras: ", size: 14.0, bold: true)
        pd.attributedFont(text: product.code ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Laboratorio: ", size: 14.0, bold: true)
        pd.attributedFont(text: product.sponsor ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        
        if (product.presentation?.isEmpty == false) {
            pd.attributedFont(text: "Presentación: ", size: 14.0, bold: true)
            pd.attributedFont(text: product.presentation ?? "", size: 14.0)
        }
        
        scanResultLabel.attributedText = pd
        showMedicineInfo()
    }
    
    func centinelaMedicineFound(_ medicine: CentinelaMedicine) {
        idCentinelaMedicine = medicine.idCentinelaMedicine
        
        let pd = NSMutableAttributedString()
        pd.attributedFont(text: "Nombre: ", size: 14.0, bold: true)
        pd.attributedFont(text: medicine.name ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Código de Barras: ", size: 14.0, bold: true)
        pd.attributedFont(text: medicine.code ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Laboratorio: ", size: 14.0, bold: true)
        pd.attributedFont(text: medicine.sponsor ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        
        if (medicine.presentation?.isEmpty == false) {
            pd.attributedFont(text: "Presentación: ", size: 14.0, bold: true)
            pd.attributedFont(text: medicine.presentation ?? "", size: 14.0)
        }
        
        scanResultLabel.attributedText = pd
        showMedicineInfo()
    }
    
    func showMedicineInfo() {
        scanResultLabel.alpha = 0.0
        scanResultLabel.isHidden = false
        scanRequestContainer.alpha = 0.0
        scanRequestContainer.isHidden = false
        activityIndicator.stopInderminateAnimation()
        
        scanResultLabel.frame.size.height = scanResultLabel.sizeThatFits(CGSize(width: scanResultLabel.frame.size.width, height: .greatestFiniteMagnitude)).height
        productInfoContainer.reorder(direction: .vertical, resize: true, ignoreFilters: [[ViewFilterParameter.equals : activityIndicator]], offset: ViewOffset(bottom: 10.0, left: 0.0, right: 0.0, top: 10.0), spacing: 20.0, initialMargin: 10.0)
        scanRequestContainer.frame.size = productInfoContainer.frame.size
        scanRequestContainer.center = view.center
        continueButton.center.x = scanRequestContainer.bounds.width / 2.0
        
        continueButton.setVisible(true, animated: false, onComplete: nil)
        scanResultLabel.setVisible(true, animated: false, onComplete: nil)
        productInfoContainer.setVisible(true, animated: false, onComplete: nil)
        scanRequestContainer.setVisible(true, animated: true, onComplete: nil)
        
        contentScroll.setVisible(true, animated: true, onComplete: nil)
    }
}

extension SelectEntryType: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.Naat.borderGray.cgColor, showDefaultBorder: true, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.keyboardType == .numberPad {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            let onlyNumbers = string == numberFiltered
            
            if textField.isEqual(barcodeField) {
                let currentCharacterCount = textField.text?.count ?? 0
                if (range.length + range.location > currentCharacterCount){
                    return false
                }
                
                let newLength = currentCharacterCount + string.count - range.length
                return newLength <= 14
            }
            
            return onlyNumbers
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
            textField.text = ""
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.Naat.borderGray.cgColor, showDefaultBorder: true, animated: true)
        
        if textField.isEqual(barcodeField) && textField.text?.trimmingCharacters(in: .whitespaces).isEmpty == false {
            tryToFindProduct()
        }
    }
}
