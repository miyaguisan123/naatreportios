//
//  ScanCode.swift
//  NaatReport
//
//  Created by System on 11/30/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit
import AVFoundation
import Foundation

class ScanCode: BaseViewController {
    @IBOutlet weak var top: UIView!
    @IBOutlet weak var flashButton: UIView!
    @IBOutlet weak var cameraContainer: UIView!
    
    @IBOutlet weak var activityIndicator: CircularActivityIndicator!
    @IBOutlet weak var continueButton: NaatButton!
    @IBOutlet weak var codeTypeLabel: UILabel!
    
    @IBOutlet weak var scanRequestContainer: UIView!
    @IBOutlet weak var scanResultLabel: UILabel!
    @IBOutlet weak var productInfoContainer: UIView!
    @IBOutlet weak var lotField: CustomUITextField!
    @IBOutlet weak var serialNumberField: CustomUITextField!
    @IBOutlet weak var expirationMonthField: CustomUITextField!
    @IBOutlet weak var expirationYearField: CustomUITextField!
    
    @IBOutlet weak var focusTarget: UIView!
    @IBOutlet weak var manualAdd: UIButton!
    
    var captureSession = AVCaptureSession()
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var idCentinelaPatient: String? = nil
    var idCentinelaMedicine: String? = nil
    var idCentinelaProduct: String? = nil
    
    var params = [String:Any]()
    var severity = "Leve"
    var gravity = "No Grave"
    
    deinit {
        idCentinelaPatient = nil
        idCentinelaMedicine = nil
        idCentinelaProduct = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scanRequestContainer.layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        scanRequestContainer.clipsToBounds = true
        
        focusTarget.layer.borderColor = UIColor.Naat.orange.cgColor
        focusTarget.layer.borderWidth = 1.0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupFlash()
        setupCamera()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        medicineRegisterDidCancel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        destroyCamera()
    }
    
    func addNotification(expirationDate: Date, lot: String, serialNum: String) {
        lotField.resignFirstResponder()
        expirationYearField.resignFirstResponder()
        expirationMonthField.resignFirstResponder()
        
        params["idCentinelaPatient"] = self.idCentinelaPatient ?? "0"
        params["idCentinelaMedicine"] = self.idCentinelaMedicine ?? "0"
        params["idCentinelaProduct"] = self.idCentinelaProduct ?? "0"
        params["expiration_date"] = expirationDate.stringInFormat(.slashedNumericStandard)
        params["lot"] = lot
        params["serial_num"] = serialNum
        params["notes"] = "Registered via APP"
        params["origin"] = "NAAT REPORT"
        
        let extras = AddNotificationExtras()
        extras.delegate = self
        extras.modalPresentationStyle = .overCurrentContext
        extras.automaticallyAdjustsScrollViewInsets = false
        
        present(extras, animated: true, completion: {})
    }
    
    @IBAction func close() {
        destroyCamera()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func toggleTorch(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else {
            return
        }
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if sender.isSelected == true {
                    device.torchMode = .on
                }
                else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    @IBAction func createReport() {
        var formIsValid = true
        
        for view in scanRequestContainer.subviews {
            guard let field = view as? CustomUITextField else { continue }
            field.resignFirstResponder()
            if field.text?.trimmingCharacters(in: .whitespaces).isEmpty == true {
                field.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                formIsValid = false
            }
        }
        
        if formIsValid == false { return }
        
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_ENTER,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.scanRequestContainer.alpha = 0.0
        }) { (finish) in
            let year = self.expirationYearField.text ?? "0"
            let month = self.expirationMonthField.text ?? "0"
            
            let userCalendar = Calendar.current
            var expirationDateComponents = DateComponents()
            expirationDateComponents.year = Int(year)
            expirationDateComponents.month = Int(month)
            expirationDateComponents.day = 1
            
            if let expirationDate = userCalendar.date(from: expirationDateComponents) {
                self.addNotification(expirationDate: expirationDate, lot: self.lotField.text ?? "", serialNum: self.serialNumberField.text ?? "")
            }
            else {
                self.expirationYearField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
                self.expirationMonthField.set(mode: .error, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
            }
        }
    }
    
    @IBAction func manuallyAddProduct() {
        manualAdd.setVisible(false, animated: true, onComplete: nil)
        medicineNotFound("", "", "", "", "")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewReport = segue.destination as? ViewReport, let firstFormInReport = sender as? FormInReport {
            viewReport.idFormInReport = firstFormInReport.idFormInReport
        }
        else if let scanCode = segue.destination as? ScanCode {
            scanCode.idCentinelaPatient = idCentinelaPatient
        }
        else if let pickForm = segue.destination as? FormPicker, let report = sender as? CentinelaReport {
            pickForm.idCentinelaReport = report.idCentinelaReport
        }
    }
}

extension ScanCode: AVCaptureMetadataOutputObjectsDelegate {
    func destroyCamera() {
        if captureSession.isRunning == true {
            captureSession.stopRunning()
        }
        
        if previewLayer != nil {
            previewLayer?.removeFromSuperlayer()
            previewLayer = nil
        }
    }
    
    func setupCamera() {
        guard previewLayer == nil else { return }
        
        top.addDropShadow(opacity: 0.3)
        let device = AVCaptureDevice.default(for: AVMediaType.video)
        let input: AVCaptureDeviceInput
        
        do {
            input = try AVCaptureDeviceInput(device: device!)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(input)) {
            captureSession.addInput(input)
        } else {
            return
        }
        
        let output = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(output)) {
            captureSession.addOutput(output)
            
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.ean8,
                                          AVMetadataObject.ObjectType.ean13,
                                          AVMetadataObject.ObjectType.pdf417,
                                          AVMetadataObject.ObjectType.dataMatrix,
            AVMetadataObject.ObjectType.qr]
        } else {
            return
        }
        
        let pl = AVCaptureVideoPreviewLayer(session: captureSession)
        pl.frame = cameraContainer.bounds
        pl.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraContainer.layer.addSublayer(pl)
        previewLayer = pl
    }
    
    func setupFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        
        if device.hasTorch {
            flashButton.isHidden = false
        }
    }
    
    internal func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        for metadata in metadataObjects {
            if let readableObject = metadata as? AVMetadataMachineReadableCodeObject {
                loadWSFromProductoWith(code: readableObject.stringValue!, type: readableObject.type.rawValue)
                break
            }
        }
    }
}

extension ScanCode {
    func loadWSFromProductoWith(code: String, type: String) {
        var barcode = code
        var caducity: String? = nil
        var month: String? = nil
        var year: String? = nil
        var lot: String? = nil
        var serialNumber: String? = nil
        
        if type == "org.iso.DataMatrix" {
            let caducityPattern = "17\\d{6}10"
            if let caducityRange = code.range(of: caducityPattern, options: .regularExpression) {
                caducity = String(code[caducityRange].dropFirst(2).dropLast(2))
                barcode = String(code[..<caducityRange.lowerBound].dropFirst(3))
                
                let lotAndSerial = String(code[caducityRange.upperBound...])
                let lotSeparator = (lotAndSerial.range(of: "21", options: .backwards))!
                lot = String(lotAndSerial[..<lotSeparator.lowerBound].dropLast())
                serialNumber = String(lotAndSerial[lotSeparator.upperBound...])
            }
        }
        
        if captureSession.isRunning == true {
            captureSession.stopRunning()
        }
        
        codeTypeLabel.text = "Código: \(barcode)"
        lotField.text = lot
        serialNumberField.text = serialNumber
        
        if let caducity = caducity {
            month = String(caducity.dropFirst(2).dropLast(2))
            year = String(caducity.dropLast(4))
            
            expirationMonthField.text = month
            expirationYearField.text = year
        }
        
        manualAdd.setVisible(false, animated: true, onComplete: nil)
        scanRequestContainer.setVisible(true, animated: true) {
            Alamofire.request("\(API)/medicines/\(barcode)?type=barcode", method: .get, parameters: nil, encoding: URLEncoding(), headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
                switch response.result {
                case .success (let string):
                    if self.reviewTokenConflict(string) { return }
                    
                    guard let medicineInfo = string.json() as? [AnyHashable:Any], medicineInfo.keys.contains("id"), let type = medicineInfo["type"] as? String else {
                        self.medicineNotFound(barcode, lot, serialNumber, month, year)
                        return
                    }
                    
                    if type == "product", let product = DataBaseManager.updateCentinelaProduct(medicineInfo, notification: nil) {
                        self.centinelaProductFound(product)
                    }
                    else if type == "medicine", let medicine = DataBaseManager.updateCentinelaMedicine(medicineInfo, notification: nil) {
                        self.centinelaMedicineFound(medicine)
                    }
                    
                    break
                case .failure:
                    self.networkRequestFailed()
                    self.medicineRegisterDidCancel()
                    break
                }
            })
        }
    }
    
    func medicineNotFound(_ barCode: String, _ lot: String?, _ serialNumber: String?, _ month: String?, _ year: String?) {
        scanRequestContainer.isHidden = true
        
        let rm = RegisterMedicine()
        rm.delegate = self
        rm.medicineBarCode = barCode
        rm.medicineLot = lot
        rm.medicineSerialNumber = serialNumber
        rm.medicineExpirationMonth = month
        rm.medicineExpirationYear = year
        rm.modalPresentationStyle = .overCurrentContext
        
        present(rm, animated: true, completion: nil)
    }
    
    func centinelaProductFound(_ product: CentinelaProduct) {
        idCentinelaProduct = product.idCentinelaProduct
        
        let pd = NSMutableAttributedString()
        pd.attributedFont(text: "Nombre: ", size: 14.0, bold: true)
        pd.attributedFont(text: product.name ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Código de Barras: ", size: 14.0, bold: true)
        pd.attributedFont(text: product.code ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Laboratorio: ", size: 14.0, bold: true)
        pd.attributedFont(text: product.sponsor ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        
        if (product.presentation?.isEmpty == false) {
            pd.attributedFont(text: "Presentación: ", size: 14.0, bold: true)
            pd.attributedFont(text: product.presentation ?? "", size: 14.0)
        }
        
        scanResultLabel.attributedText = pd
        showMedicineInfo()
    }
    
    func centinelaMedicineFound(_ medicine: CentinelaMedicine) {
        idCentinelaMedicine = medicine.idCentinelaMedicine
        
        let pd = NSMutableAttributedString()
        pd.attributedFont(text: "Nombre: ", size: 14.0, bold: true)
        pd.attributedFont(text: medicine.name ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Código de Barras: ", size: 14.0, bold: true)
        pd.attributedFont(text: medicine.code ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        pd.attributedFont(text: "Laboratorio: ", size: 14.0, bold: true)
        pd.attributedFont(text: medicine.sponsor ?? "", size: 14.0)
        pd.attributedFont(text: "\n")
        
        if (medicine.presentation?.isEmpty == false) {
            pd.attributedFont(text: "Presentación: ", size: 14.0, bold: true)
            pd.attributedFont(text: medicine.presentation ?? "", size: 14.0)
        }
        
        scanResultLabel.attributedText = pd
        showMedicineInfo()
    }
    
    func showMedicineInfo() {
        codeTypeLabel.alpha = 0.0
        codeTypeLabel.isHidden = false
        continueButton.alpha = 0.0
        continueButton.isHidden = false
        scanResultLabel.alpha = 0.0
        scanResultLabel.isHidden = false
        scanRequestContainer.alpha = 0.0
        scanRequestContainer.isHidden = false
        activityIndicator.stopInderminateAnimation()
        
        scanResultLabel.frame.size.height = scanResultLabel.sizeThatFits(CGSize(width: codeTypeLabel.frame.size.width, height: .greatestFiniteMagnitude)).height
        productInfoContainer.reorder(direction: .vertical, resize: true, ignoreFilters: [[ViewFilterParameter.equals : activityIndicator]], offset: ViewOffset(bottom: 10.0, left: 0.0, right: 0.0, top: 10.0), spacing: 20.0, initialMargin: 10.0)
        scanRequestContainer.frame.size = productInfoContainer.frame.size
        scanRequestContainer.center = view.center
        continueButton.center.x = scanRequestContainer.bounds.width / 2.0
        
        codeTypeLabel.setVisible(true, animated: false, onComplete: nil)
        continueButton.setVisible(true, animated: false, onComplete: nil)
        scanResultLabel.setVisible(true, animated: false, onComplete: nil)
        productInfoContainer.setVisible(true, animated: false, onComplete: nil)
        scanRequestContainer.setVisible(true, animated: true, onComplete: nil)
    }
}

extension ScanCode: UITextFieldDelegate {
    internal func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.set(mode: .editing, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(lotField) {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 25
        }
        
        if textField.keyboardType == .numberPad {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        
        return true
    }
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        textField.set(mode: .normal, defaultBackgroundColor: .white, defaultBorderColor: UIColor.clear.cgColor, showDefaultBorder: false, animated: true)
    }
}

extension ScanCode: RegisterMedicineDelegate {
    func medicineRegisterDidCancel() {
        view.endEditing(true)
        manualAdd.setVisible(true, animated: true, onComplete: nil)
        productInfoContainer.setVisible(false, animated: false, onComplete: nil)
        
        scanRequestContainer.setVisible(false, animated: false) {
            if (self.captureSession.isRunning == false) {
                self.captureSession.startRunning()
            }
        }
    }
    
    func medicineDidRegister(_ medicine: CentinelaMedicine, lot: String, expirationDate: Date, serialNum: String, origin: String, notes: String) {
        productInfoContainer.setVisible(false, animated: false, onComplete: nil)
        scanRequestContainer.setVisible(false, animated: false, onComplete: nil)
        
        idCentinelaMedicine = medicine.idCentinelaMedicine
        addNotification(expirationDate: expirationDate, lot: lot, serialNum: serialNum)
    }
}

extension ScanCode: AddNotificationExtrasDelegate {
    func extrasDidSelect(severity: String, gravity: String) {
        params["severity"] = severity
        params["gravity"] = gravity
        
        Alamofire.request("\(API)/addNotification", method: .post, parameters: params, encoding: JSONEncoding.default, headers: UtilitySession.defaultHeaders()).responseString(completionHandler: { (response) in
            switch response.result {
            case .success (let string):
                if self.reviewTokenConflict(string) { return }
                
                guard let notificationInfo = string.json() as? [AnyHashable:Any] else {
                    self.medicineRegisterDidCancel()
                    self.networkRequestFailed()
                    return
                }
                
                if let notification = DataBaseManager.updateNotificationReport(notificationInfo) {
                    let centinelaUsers = DataBaseManager.fetchObjects(objectName: "CentinelaUser") as? [CentinelaUser]
                    let report = notification.centinelaReport
                    
                    if centinelaUsers?.first != nil {
                        self.performSegue(withIdentifier: "PickForm", sender: report)
                    }
                    else {
                        if let firstForm = report?.forms?.allObjects.first as? FormInReport {
                            self.performSegue(withIdentifier: "ViewForm", sender: firstForm)
                        }
                    }
                }
                
                break
            case .failure:
                self.networkRequestFailed()
                self.close()
                break
            }
        })
    }
}
