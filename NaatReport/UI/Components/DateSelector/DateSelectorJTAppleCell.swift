import JTAppleCalendar

class DateSelectorJTAppleCell: JTAppleCell {
    @IBOutlet weak var currentDayIndicator: UIView!
    @IBOutlet weak var selectedIndicator: UIView!
    @IBOutlet weak var dayLabel: UILabel!
}
