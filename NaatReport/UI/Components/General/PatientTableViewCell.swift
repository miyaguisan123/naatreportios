//
//  PatientTableViewCell.swift
//  NaatReport
//
//  Created by Miyagui on 11/23/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

class PatientTableViewCell: UITableViewCell {
    @IBOutlet weak var content: UIView!
    @IBOutlet weak var avatar: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var notificationCounterLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatar.clipsToBounds = true
        avatar.layer.cornerRadius = avatar.bounds.height / 2.0
        avatar.layer.borderColor = UIColor.Naat.orange.cgColor
        avatar.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        avatar.sd_cancelImageLoad(for: .normal)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        content.backgroundColor = highlighted ? UIColor.Naat.lightOrange : UIColor.white
        avatar.backgroundColor = highlighted ? UIColor.Naat.lightOrange : UIColor.white
        avatar.layer.borderColor = highlighted ? UIColor.white.cgColor : UIColor.Naat.orange.cgColor
        avatar.setTitleColor((highlighted ? UIColor.white : UIColor.Naat.orange), for: .normal)
        nameLabel.backgroundColor = highlighted ? UIColor.Naat.lightOrange : UIColor.white
        nameLabel.textColor = highlighted ? UIColor.white : UIColor.Naat.textColor
        notificationCounterLabel.backgroundColor = highlighted ? UIColor.white : UIColor.Naat.lightOrange
        notificationCounterLabel.textColor = highlighted ? UIColor.Naat.orange : UIColor.white
    }
}
