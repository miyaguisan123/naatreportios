//
//  CustomUITextView.swift
//  Buckets
//
//  Created by Miyagui on 2/9/18.
//  Copyright © 2018 Buckets LLC. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUITextView: UITextView {
    var defaultTextColor: UIColor? = nil
    var defaultFont: UIFont? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        defaultTextColor = textColor
        
        if let currentFont = font {
            defaultFont = UIFont(name: currentFont.fontName, size: currentFont.pointSize)
        }
    }
}
