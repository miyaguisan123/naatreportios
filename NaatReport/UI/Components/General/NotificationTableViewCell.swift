//
//  NotificationTableViewCell.swift
//  NaatReport
//
//  Created by Miyagui on 12/26/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var reportNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nextIndicator: UILabel!
    @IBOutlet weak var kindIcon: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        reportNameLabel.text = nil
        productNameLabel.text = nil
        statusLabel.text = nil
        dateLabel.text = nil
        nextIndicator.isHidden = true
    }
}
