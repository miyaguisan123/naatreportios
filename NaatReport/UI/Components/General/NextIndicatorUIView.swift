//
//  NextIndicatorUIView.swift
//  NaatReport
//
//  Created by Miyagui on 2/7/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import UIKit

protocol NextIndicatorUIViewDelegate: class {
    func nextIndicatorViewSelected(_ nextIndicatorView: NextIndicatorUIView)
}

@IBDesignable
open class NextIndicatorUIView: UIView {
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var iconContainer: UILabel!
    @IBOutlet weak var nextIndicator: UILabel!
    
    @IBOutlet weak var iconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var nextWidthConstraint: NSLayoutConstraint!
    
    weak var delegate: NextIndicatorUIViewDelegate? = nil
    var action = (() -> Void).self
    
    @IBInspectable var nextIndicatorIsVisible: Bool = true {
        didSet {
            nextWidthConstraint.constant = nextIndicatorIsVisible ? 30.0 : 0.0
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var text: String = "Label" {
        didSet {
            textLabel.text = text
        }
    }
    
    @IBInspectable var icon: String = "" {
        didSet {
            iconContainer.text = icon
            iconWidthConstraint.constant = 50.0
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var defaultBackgroundColor: UIColor = UIColor.white {
        didSet {
            mainContainer.backgroundColor = defaultBackgroundColor
        }
    }
    
    @IBInspectable var highlightColor: UIColor = UIColor.white
    @IBInspectable var highlighted = false {
        didSet {
            setActive(highlighted, animated: true, onComplete: nil)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupNibView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupNibView()
    }
    
    func setupNibView() {
        if self.subviews.count == 0 {
            let xibView = self.loadNib()
            xibView.frame = bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            addSubview(xibView)
        }
    }
    
    private func loadNib() -> UIView {
        let dynamicType = type(of: self)
        let bundle = Bundle(for: dynamicType)
        let nib = UINib(nibName: self.nibName(), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func nibName() -> String {
        let dynamicType = type(of: self).description()
        return dynamicType.components(separatedBy: ".").last!
    }
    
    func setActive(_ shouldBeActive: Bool, animated: Bool, delay: TimeInterval = 0.0, onComplete: (() -> Void)?) {
        UIView.animate(withDuration: animated ? ANIMATION_DEFAULT_DURATION_EXIT : 0.0,
                       delay: delay,
                       options: .beginFromCurrentState,
                       animations: {
                        self.mainContainer.backgroundColor = shouldBeActive ? self.highlightColor : self.defaultBackgroundColor
        }) { (finish) in
            if onComplete != nil {
                onComplete!()
            }
        }
    }
    
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        nextIndicatorIsVisible = true
    }
}

extension NextIndicatorUIView: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    @IBAction func handleTap(_ gesture: UITapGestureRecognizer) {
        if gesture.state == .recognized {
            setActive(true, animated: true, onComplete: {
                self.delegate?.nextIndicatorViewSelected(self)
                self.setActive(false, animated: true, onComplete: nil)
            })
        }
    }
}
