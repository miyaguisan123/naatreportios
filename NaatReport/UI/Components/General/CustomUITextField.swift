//
//  CustomUITextField.swift
//  Buckets
//
//  Created by Miyagui on 6/3/17.
//  Copyright © 2017 Buclets LLC. All rights reserved.
//

import UIKit

@IBDesignable
class CustomUITextField: UITextField {
    var defaultTextColor: UIColor? = nil
    var defaultFont: UIFont? = nil
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        super.textRect(forBounds: bounds)
        
        return CGRect(x: bounds.origin.x + 5.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        super.editingRect(forBounds: bounds)
        
        return CGRect(x: bounds.origin.x + 5.0, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        layer.cornerRadius = UI_DEFAULT_CORNER_RADIUS
        defaultTextColor = textColor
        
        if let currentFont = font {
            defaultFont = UIFont(name: currentFont.fontName, size: currentFont.pointSize)
        }
    }
}
