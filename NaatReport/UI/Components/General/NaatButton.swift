//
//  NaatButton.swift
//  NaatReport
//
//  Created by System on 11/30/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

class NaatButton: UIButton {
    var highlightBackgroundColor = UIColor.Naat.lightOrange
    var normalBackgroundColor = UIColor.Naat.orange {
        didSet {
            self.setNormalBackground()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        isOpaque = true
        backgroundColor = UIColor.Naat.orange
        
        addTarget(self, action: #selector(self.setHighlightBackground), for: .touchDown)
        addTarget(self, action: #selector(self.setNormalBackground), for: .touchUpInside)
        
        layer.cornerRadius = bounds.height / 2.0
    }
    
    @objc func setHighlightBackground() {
        backgroundColor = highlightBackgroundColor
    }
    
    @objc func setNormalBackground() {
        backgroundColor = normalBackgroundColor
    }
}
