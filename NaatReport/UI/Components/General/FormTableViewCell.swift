//
//  FormTableViewCell.swift
//  NaatReport
//
//  Created by Miyagui on 2/22/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import UIKit

protocol FormTableViewCellDelegate: class {
    func formRequestToAdd(_ cell: FormTableViewCell)
}

class FormTableViewCell: UITableViewCell {
    @IBOutlet weak var formNameLabel: UILabel!
    @IBOutlet weak var formAddButton: UIButton!
    
    weak var delegate: FormTableViewCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        formNameLabel.text = nil
        formAddButton.isHidden = true
    }
    
    @IBAction func pressMe() {
        delegate?.formRequestToAdd(self)
    }
}
