//
//  UtilitySession.swift
//  NaatReport
//
//  Created by Miyagui on 2/7/18.
//  Copyright © 2018 Digitalizatxt. All rights reserved.
//

import Alamofire
import UIKit

class UtilitySession: NSObject {
    public class func defaultHeaders() -> HTTPHeaders {
        var headers: HTTPHeaders = [:]
        headers["token"] = UtilitySession.getSessionToken()
        headers["Accept"] = "application/json, text/javascript, */*; q=0.01"
        headers["Content-type"] = "application/json"
        
        return headers
    }
    
    public class func getSystemCentinelaUser() -> CentinelaUser? {
        if let allUsers = DataBaseManager.fetchObjects(objectName: "CentinelaUser", filter: nil, sortingMethods: [], context: DataBaseManager.context()) as? [CentinelaUser] {
            return allUsers.first
        }
        
        return nil
    }
    
    public class func getSystemDoctor() -> ApplicationUser? {
        if let allAppUsers = DataBaseManager.fetchObjects(objectName: "ApplicationUser", filter: "type LIKE[C] 'doctor'", sortingMethods: [], context: DataBaseManager.context()) as? [ApplicationUser] {
            return allAppUsers.first
        }
        
        return nil
    }
    
    public class func setSessionToken(_ token: String?) {
        UtilitySession.setSessionVar(key: "token", value: token)
    }
    
    public class func getSessionToken() -> String {
        if let token = UtilitySession.getSessionVar(key: "token") as? String {
            return token
        }
        
        return ""
    }
    
    public class func setSessionVar(key: String, value: Any?) {
        let defaults = UserDefaults.standard
        
        if value != nil {
            defaults.set(value!, forKey: key)
        }
        else {
            defaults.removeObject(forKey: key)
        }
        
        defaults.synchronize()
    }
    
    public class func getSessionVar(key: String) -> Any? {
        return UserDefaults.standard.object(forKey: key)
    }
}
