//
//  UtilityGeneral.swift
//  NaatReport
//
//  Created by Miyagui on 11/23/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import UIKit

let API = "http://192.168.1.147:8084/naatcentinela/api"
let API_DEFAULT_HEADERS = ["Content-Type":"application/json"]

extension String {
    func json() -> Any? {
        if let data = self.data(using: String.Encoding.utf8) {
            if let json = try? JSONSerialization.jsonObject(with: data) {
                return json
            }
        }
        
        return nil
    }
}

extension Bundle {
    var releaseVersionNumber: String? { return infoDictionary?["CFBundleShortVersionString"] as? String }
    var buildVersionNumber: String? { return infoDictionary?["CFBundleVersion"] as? String }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        
        return controller
    }
    
    public class func getVisibleViewController() -> UIViewController? {
        return UIApplication.topViewController()
    }
}

extension Float {
    public var g: CGFloat {
        return CGFloat(self)
    }
    public var d: Double {
        return Double(self)
    }
}

extension Double {
    public var g: CGFloat {
        return CGFloat(self)
    }
    public var f: Float {
        return Float(self)
    }
}

extension CGFloat {
    public var d: Double {
        return Double(self)
    }
    public var f: Float {
        return Float(self)
    }
}
