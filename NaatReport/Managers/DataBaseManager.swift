//
//  DataBaseManager.swift
//  NaatReport
//
//  Created by System on 11/30/17.
//  Copyright © 2017 Digitalizatxt. All rights reserved.
//

import CoreData
import UIKit

class DataBaseManager: NSObject {
    public class func context() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.databaseContext
    }
    
    public class func saveContext() {
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    public class func clearDataBase() {
        let context = DataBaseManager.context()
        let currentPolicy = context.mergePolicy
        
        context.reset()
        DataBaseManager.deleteAllDataFromModel("ApplicationUser")
        DataBaseManager.deleteAllDataFromModel("CentinelaMedicine")
        DataBaseManager.deleteAllDataFromModel("CentinelaPatient")
        DataBaseManager.deleteAllDataFromModel("CentinelaReport")
        DataBaseManager.deleteAllDataFromModel("CentinelaUser")
        DataBaseManager.deleteAllDataFromModel("NotificationReport")
        
        DataBaseManager.saveContext()
        context.mergePolicy = currentPolicy
    }
}

extension DataBaseManager {
    public class func deleteAllDataFromModel(_ objectName: String) {
        let batchDelete = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: objectName))
        
        do {
            try DataBaseManager.context().execute(batchDelete)
            DataBaseManager.saveContext()
        } catch {
            //error
        }
    }
    
    public class func fetchObjects(objectName: String, filter: String? = nil, sortingMethods: [String] = [String](), context: NSManagedObjectContext = DataBaseManager.context()) -> Array<Any> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: objectName)
        
        if filter != nil && filter?.isEmpty == false {
            fetchRequest.predicate = NSPredicate(format: filter!)
        }
        
        var sortDescriptors = [NSSortDescriptor]()
        for sortingProperty in sortingMethods {
            let descriptor = NSSortDescriptor(key: sortingProperty, ascending: true)
            sortDescriptors.append(descriptor)
        }
        
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let results = try context.fetch(fetchRequest)
            return results
        } catch let error {
            print("error", error)
        }
        
        return []
    }
    
    public class func get(_ className: String, id: String?, context: NSManagedObjectContext = DataBaseManager.context()) -> Any? {
        var object: Any? = nil
        
        if id != nil && id?.isEmpty == false {
            let result = DataBaseManager.fetchObjects(objectName: className, filter: "id\(className) LIKE[c] '\(id!)'", sortingMethods: [], context: context)
            
            if result.count > 0 {
                object = result.first
            }
        }
        
        return object
    }
    
    public class func find(_ className: String, filter: String, context: NSManagedObjectContext = DataBaseManager.context()) -> Any? {
        var object: Any? = nil
        let result = DataBaseManager.fetchObjects(objectName: className, filter: filter, sortingMethods: [], context: context)
        
        if result.count > 0 {
            object = result.first
        }
        
        return object
    }
    
    public class func getId(from id: Any?) -> String? {
        if id != nil && id is String && (id as! String).isEmpty == false {
            return id as? String
        }
        else if id != nil && id is NSNumber {
            return "\((id as! NSNumber).intValue)"
        }
        
        return nil
    }
}


extension DataBaseManager {
    @discardableResult public class func updateCentinelaUser(_ WSJSON: [AnyHashable:Any], context: NSManagedObjectContext = DataBaseManager.context()) -> CentinelaUser? {
        guard let idCentinelaUser = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        
        var user = DataBaseManager.get("CentinelaUser", id: idCentinelaUser, context: context) as? CentinelaUser
        if user == nil {
            if #available(iOS 10.0, *) {
                user = CentinelaUser(context: context)
            } else {
                user = NSEntityDescription.insertNewObject(forEntityName: "CentinelaUser", into: context) as? CentinelaUser
            }
            
            user?.idCentinelaUser = idCentinelaUser
        }
        
        user?.phone = (WSJSON["phone"] as? String) ?? ""
        user?.email = (WSJSON["email"] as? String) ?? ""
        user?.username = (WSJSON["username"] as? String) ?? ""
        user?.lastnameFather = (WSJSON["father_lastname"] as? String) ?? ""
        user?.lastnameMother = (WSJSON["mother_lastname"] as? String) ?? ""
        user?.name = (WSJSON["name"] as? String) ?? ""
        user?.dateCreation = (WSJSON["creation_date"] as? String)?.date(from: .iso8601) ?? Date()
        
        if let patients = WSJSON["patients"] as? [[AnyHashable:Any]] {
            for patientWSJSON in patients {
                updateCentinelaPatient(patientWSJSON)
            }
        }
        
        try? context.save()
        
        return user
    }
    
    @discardableResult public class func updateCentinelaPatient(_ WSJSON: [AnyHashable:Any]) -> CentinelaPatient? {
        guard let idCentinelaPatient = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        
        let context = DataBaseManager.context()
        var patient = DataBaseManager.get("CentinelaPatient", id: idCentinelaPatient, context: context) as? CentinelaPatient
        if patient == nil {
            if #available(iOS 10.0, *) {
                patient = CentinelaPatient(context: context)
            } else {
                patient = NSEntityDescription.insertNewObject(forEntityName: "CentinelaPatient", into: context) as? CentinelaPatient
            }
            
            patient?.idCentinelaPatient = idCentinelaPatient
        }
        
        patient?.dateBirth = (WSJSON["birth_date"] as? String)?.date(from: .iso8601) ?? Date()
        patient?.gender = (WSJSON["gender"] as? NSNumber) ?? NSNumber(value: true)
        patient?.lastnameFather = (WSJSON["father_lastname"] as? String) ?? ""
        patient?.lastnameMother = (WSJSON["mother_lastname"] as? String) ?? ""
        patient?.name = (WSJSON["name"] as? String) ?? ""
        patient?.rfc = (WSJSON["rfc"] as? String) ?? ""
        patient?.curp = (WSJSON["curp"] as? String) ?? ""
        patient?.phone = (WSJSON["phone"] as? String) ?? ""
        
        let zipCode = WSJSON["zipCode"] as? Int ?? 0
        patient?.zipCode = NSNumber(value: zipCode)
        
        if let cityWSJSON = WSJSON["city"] as? [AnyHashable:Any] {
            patient?.idCity = (cityWSJSON["id"] as? NSNumber) ?? NSNumber(value: 0)
            patient?.cityName = (cityWSJSON["name"] as? String) ?? ""
        }
        
        if let stateWSJSON = WSJSON["state"] as? [AnyHashable:Any] {
            patient?.idState = (stateWSJSON["id"] as? NSNumber) ?? NSNumber(value: 0)
            patient?.stateName = (stateWSJSON["name"] as? String) ?? ""
        }
        
        if let neighborhoodWSJSON = WSJSON["neighborhood"] as? [AnyHashable:Any] {
            patient?.idHood = (neighborhoodWSJSON["id"] as? NSNumber) ?? NSNumber(value: 0)
            patient?.hoodName = (neighborhoodWSJSON["name"] as? String) ?? ""
        }
        
        patient?.professional = UtilitySession.getSystemCentinelaUser()
        patient?.doctor = UtilitySession.getSystemDoctor()
        
        try? context.save()
        
        return patient
    }
    
    @discardableResult public class func updateNotificationReport(_ WSJSON: [AnyHashable:Any]) -> NotificationReport? {
        guard let idNotificationReport = WSJSON["idNotificationReport"] as? [AnyHashable:Any],
            let idNotification = (idNotificationReport["idNotificationReport"] as? NSNumber)?.intValue,
            let idReport = (idNotificationReport["idReport"] as? NSNumber)?.intValue,
            let reportInfo = WSJSON["report"] as? [AnyHashable:Any]
            else {
                return nil
        }
        
        let context = DataBaseManager.context()
        let id = "\(idNotification),\(idReport)"
        
        var notification = DataBaseManager.get("NotificationReport", id: id) as? NotificationReport
        if notification == nil {
            if #available(iOS 10.0, *) {
                notification = NotificationReport(context: context)
            } else {
                notification = NSEntityDescription.insertNewObject(forEntityName: "NotificationReport", into: context) as? NotificationReport
            }
            
            notification?.idNotificationReport = id
            
            if let patientInfo = WSJSON["patient"] as? [AnyHashable:Any] {
                notification?.centinelaPatient = DataBaseManager.updateCentinelaPatient(patientInfo)
            }
        }
        
        if let appUser = WSJSON["appUser"] as? [AnyHashable:Any] {
            notification?.appUser = DataBaseManager.updateApplicationUser(appUser)
        }
        
        if let medicine = WSJSON["medicine"] as? [AnyHashable:Any] {
            notification?.centinelaMedicine = DataBaseManager.updateCentinelaMedicine(medicine, notification: notification)
        }
        
        if let product = WSJSON["product"] as? [AnyHashable:Any] {
            notification?.centinelaProduct = DataBaseManager.updateCentinelaProduct(product, notification: notification)
        }
        
        if let updatedReport = DataBaseManager.updateCentinelaReport(reportInfo) {
            if let forms = WSJSON["formsInReport"] as? [[AnyHashable:Any]] {
                for formJSON in forms {
                    DataBaseManager.updateFormInReport(formJSON, report: updatedReport)
                }
            }
            
            notification?.centinelaReport = updatedReport
        }
        
        try? context.save()
        
        return notification
    }
    
    @discardableResult public class func updateCentinelaReport(_ WSJSON: [AnyHashable:Any]) -> CentinelaReport? {
        guard let idCentinelaReport = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        let context = DataBaseManager.context()
        
        var report = DataBaseManager.get("CentinelaReport", id: idCentinelaReport, context: context) as? CentinelaReport
        if report == nil {
            if #available(iOS 10.0, *) {
                report = CentinelaReport(context: context)
            } else {
                report = NSEntityDescription.insertNewObject(forEntityName: "CentinelaReport", into: context) as? CentinelaReport
            }
            
            report?.idCentinelaReport = idCentinelaReport
        }
        
        report?.file = WSJSON["file"] as? String ?? ""
        report?.name = WSJSON["name"] as? String ?? ""
        report?.status = (WSJSON["status"] as? NSNumber) ?? NSNumber(value: 1)
        report?.classification = (WSJSON["classification"] as? String) ?? "notifications"
        report?.additionalClassification = (WSJSON["additional_classification"] as? String) ?? "SRAM"
        report?.causality = WSJSON["causality"] as? String
        report?.phase = (WSJSON["phase"] as? NSNumber) ?? NSNumber(value: 1)
        report?.grave = (WSJSON["grave"] as? NSNumber) ?? NSNumber(value: false)
        report?.severity = (WSJSON["severity"] as? String) ?? "leve"
        report?.adverseReaction = (WSJSON["adverse_reaction"] as? String) ?? "probable"
        report?.type = (WSJSON["type"] as? String) ?? "espontaneo"
        report?.quality = (WSJSON["quality"] as? NSNumber) ?? NSNumber(value: 1)
        report?.category = (WSJSON["category"] as? String) ?? ""
        report?.origin = WSJSON["origin"] as? String ?? "Centinela"
        
        report?.dateRegister = (WSJSON["register_date"] as? String)?.date(from: .iso8601)
        report?.dateSent = (WSJSON["sent_date"] as? String)?.date(from: .iso8601)
        report?.dateVerified = (WSJSON["verified_date"] as? String)?.date(from: .iso8601)
        report?.dateEnd = (WSJSON["end_date"] as? String)?.date(from: .iso8601)
        report?.dateStart = (WSJSON["start_date"] as? String)?.date(from: .iso8601)
        
        if WSJSON.keys.contains("unit") {
            let unitInfo = WSJSON["unit"] as? [AnyHashable:Any]
            if let idUnit = DataBaseManager.getId(from: unitInfo?["id"]) {
                report?.idUnit = idUnit
            }
        }
        
        if WSJSON.keys.contains("site") {
            let siteInfo = WSJSON["site"] as? [AnyHashable:Any]
            if let idSite = DataBaseManager.getId(from: siteInfo?["id"]) {
                report?.idSite = idSite
            }
        }
        
        try? context.save()
        
        return report
    }
    
    @discardableResult public class func updateFormInReport(_ WSJSON: [AnyHashable:Any], report: CentinelaReport?) -> FormInReport? {
        guard let idFormInReport = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        
        let context = report?.managedObjectContext ?? DataBaseManager.context()
        var formInReport = DataBaseManager.get("FormInReport", id: idFormInReport) as? FormInReport
        if formInReport == nil {
            if #available(iOS 10.0, *) {
                formInReport = FormInReport(context: context)
            } else {
                formInReport = NSEntityDescription.insertNewObject(forEntityName: "FormInReport", into: context) as? FormInReport
            }
            
            formInReport?.idFormInReport = idFormInReport
        }
        
        formInReport?.creationDate = (WSJSON["created_at"] as? String)?.date(from: .iso8601) ?? Date()
        formInReport?.lastUpdate = (WSJSON["updated_at"] as? String)?.date(from: .iso8601) ?? Date()
        formInReport?.status = WSJSON["status"] as? NSNumber ?? NSNumber(value: 1)
        formInReport?.file = WSJSON["file"] as? String ?? "ERROR"
        
        if report == nil && WSJSON.keys.contains("report") {
            if let reportJSON = WSJSON["report"] as? [AnyHashable:Any], let idCentinelaReport = DataBaseManager.getId(from: reportJSON["id"]) {
                formInReport?.report = DataBaseManager.get("CentinelaReport", id: idCentinelaReport) as? CentinelaReport
            }
        }
        else {
            formInReport?.report = report
        }
        
        try? context.save()
        
        return formInReport
    }
    
    @discardableResult public class func updateCentinelaMedicine(_ WSJSON: [AnyHashable:Any], notification: NotificationReport?) -> CentinelaMedicine? {
        guard let idCentinelaMedicine = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        let context = notification?.managedObjectContext ?? DataBaseManager.context()
        
        var medicine = DataBaseManager.get("CentinelaMedicine", id: idCentinelaMedicine, context: context) as? CentinelaMedicine
        if medicine == nil {
            if #available(iOS 10.0, *) {
                medicine = CentinelaMedicine(context: context)
            } else {
                medicine = NSEntityDescription.insertNewObject(forEntityName: "CentinelaMedicine", into: context) as? CentinelaMedicine
            }
            
            medicine?.idCentinelaMedicine = idCentinelaMedicine
        }
        
        medicine?.activeSubstance = WSJSON["active_substance"] as? String ?? ""
        medicine?.classificationType = WSJSON["classification_type"] as? String ?? ""
        medicine?.code = WSJSON["barcode"] as? String ?? ""
        medicine?.dateCreation = (WSJSON["creation_date"] as? String)?.date(from: .iso8601)
        medicine?.name = WSJSON["name"] as? String ?? ""
        medicine?.note = WSJSON["note"] as? String ?? ""
        medicine?.presentation = WSJSON["presentation"] as? String ?? ""
        medicine?.formula = WSJSON["formula"] as? String ?? ""
        medicine?.status = WSJSON["status"] as? NSNumber ?? NSNumber(value: 1)
        medicine?.sponsor = WSJSON["laboratory"] as? String ?? ""
        
        let notifications = medicine?.notifications
        if notification != nil && notifications?.contains(notification!) == false {
            notifications?.adding(notification!)
            medicine?.notifications = notifications
        }
        
        try? context.save()
        
        return medicine
    }
    
    @discardableResult public class func updateCentinelaProduct(_ WSJSON: [AnyHashable:Any], notification: NotificationReport?) -> CentinelaProduct? {
        guard let idCentinelaProduct = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        let context = notification?.managedObjectContext ?? DataBaseManager.context()
        
        var product = DataBaseManager.get("CentinelaProduct", id: idCentinelaProduct) as? CentinelaProduct
        if product == nil {
            if #available(iOS 10.0, *) {
                product = CentinelaProduct(context: context)
            } else {
                product = NSEntityDescription.insertNewObject(forEntityName: "CentinelaProduct", into: context) as? CentinelaProduct
            }
            
            product?.idCentinelaProduct = idCentinelaProduct
        }
        
        product?.activeSubstance = (WSJSON["active_substance"] as? String) ?? ""
        product?.code = (WSJSON["code"] as? String) ?? ""
        product?.commercialDescription = (WSJSON["commercial_description"] as? String) ?? ""
        product?.commercialName = WSJSON["commercial_name"] as? String ?? ""
        product?.name = WSJSON["name"] as? String ?? ""
        product?.riskLevel = (WSJSON["risk_level"] as? String) ?? ""
        product?.meassureUnit = (WSJSON["meassure_unit"] as? String) ?? ""
        product?.classificationType = (WSJSON["classification_type"] as? String) ?? ""
        product?.dateCreation = (WSJSON["creation_date"] as? String)?.date(from: .iso8601) ?? Date()
        product?.classificationType = WSJSON["classification_type"] as? String ?? ""
        product?.riskLevel = WSJSON["risk_level"] as? String ?? ""
        product?.sponsor = WSJSON["sponsor"] as? String ?? ""
        product?.presentation = WSJSON["presentation"] as? String ?? ""
        
        let notifications = product?.notifications
        if notification != nil && notifications?.contains(notification!) == false {
            notifications?.adding(notification!)
            product?.notifications = notifications
        }
        
        try? context.save()
        
        return product
    }
}

extension DataBaseManager {
    @discardableResult public class func updateApplicationUser(_ WSJSON: [AnyHashable:Any], context: NSManagedObjectContext = DataBaseManager.context()) -> ApplicationUser? {
        guard let idApplicationUser = DataBaseManager.getId(from: WSJSON["id"]) else { return nil }
        
        var user = DataBaseManager.get("ApplicationUser", id: idApplicationUser, context: context) as? ApplicationUser
        if user == nil {
            if #available(iOS 10.0, *) {
                user = ApplicationUser(context: context)
            } else {
                user = NSEntityDescription.insertNewObject(forEntityName: "ApplicationUser", into: context) as? ApplicationUser
            }
            
            user?.idApplicationUser = idApplicationUser
        }
        
        user?.email = (WSJSON["email"] as? String) ?? ""
        user?.username = (WSJSON["username"] as? String) ?? ""
        user?.dateCreation = (WSJSON["creation_date"] as? String)?.date(from: .iso8601) ?? Date()
        user?.dateBirth = (WSJSON["birth_date"] as? String)?.date(from: .iso8601) ?? Date()
        user?.height = (WSJSON["height"] as? NSNumber) ?? NSNumber(value: 0.0)
        user?.weight = (WSJSON["weight"] as? NSNumber) ?? NSNumber(value: 0.0)
        user?.locationDescription = (WSJSON["location_description"] as? String) ?? "";
        user?.type = WSJSON["type"] as? String ?? "patient"
        
        user?.name = (WSJSON["name"] as? String) ?? ""
        user?.lastnameFather = (WSJSON["father_lastname"] as? String) ?? ""
        user?.lastnameMother = (WSJSON["mother_lastname"] as? String) ?? ""
        user?.phone = (WSJSON["phone"] as? String) ?? ""
        
        try? context.save()
        
        return user
    }
}
