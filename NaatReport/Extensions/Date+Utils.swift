//
//  Date+Utils.swift
//  Buckets
//
//  Created by Miyagui on 2/9/18.
//  Copyright © 2018 Buckets LLC. All rights reserved.
//

import UIKit

enum DateFormat: String {
    case fullMonthNameCompact = "dd MMMM"
    case fullMonthStandard = "dd MMMM yyyy"
    case fullDate = "ddd MMMM yyyy h:mm a"
    
    case numericCompact = "dd MM"
    case numericStandard = "dd MM yyyy"
    case numericFull = "dd MM yyyy h:mm a"
    
    case slashedNumericStandard = "dd/MM/yyyy"
    case slashedNumericMonthYearh = "MM/yyyy"
    
    case slashedCompact = "dd/MMM"
    case slashedStandard = "dd/MMM/yyyy"
    case slashedFull = "dd/MMM/yyyy h:mm a"
    case slashedFullSeconds = "dd/MMM/yyyy h:mm:ss a"
    
    case monthAndYear = "MMMM yyyy"
    
    case iso8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
}

extension String {
    func date(from format: DateFormat = .iso8601) -> Date? {
        if let app = UIApplication.shared.delegate as? AppDelegate {
            let formatter = app.globalFormatter
            formatter.dateFormat = format.rawValue
            
            if format == .iso8601 {
                formatter.calendar = Calendar(identifier: .iso8601)
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.timeZone = TimeZone(secondsFromGMT: 0)
            }
            else {
                formatter.locale = Locale.current
                formatter.timeZone = TimeZone.current
            }
            
            if let date = formatter.date(from: self) {
                return date
            }
        }
        
        return nil
    }
}

extension Date {
    func stringInFormat(_ format: DateFormat) -> String? {
        if let app = UIApplication.shared.delegate as? AppDelegate {
            let formatter = app.globalFormatter
            formatter.dateFormat = format.rawValue
            
            if format == .iso8601 {
                formatter.calendar = Calendar(identifier: .iso8601)
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.timeZone = TimeZone(secondsFromGMT: 0)
            }
            else {
                formatter.locale = Locale.current
                formatter.timeZone = TimeZone.current
            }
            
            return formatter.string(from: self)
        }
        
        return nil
    }
}
