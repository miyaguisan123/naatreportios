//
//  UIView+Utils.swift
//  Buckets
//
//  Created by Miyagui on 2/9/18.
//  Copyright © 2018 Buckets LLC. All rights reserved.
//

import UIKit

enum EditableState {
    case editing
    case normal
    case error
}

let UI_DEFAULT_CORNER_RADIUS: CGFloat = 5.0
let ANIMATION_DEFAULT_DURATION_ENTER: TimeInterval = 0.225
let ANIMATION_DEFAULT_DURATION_EXIT: TimeInterval = 0.195

enum ReOrderDirection {
    case horizontal
    case vertical
}

enum ViewFilterParameter {
    case equals
    case height
    case width
}

struct ViewOffset {
    var bottom: CGFloat = 0.0
    var left:CGFloat = 0.0
    var right:CGFloat = 0.0
    var top: CGFloat = 0.0
}

extension UIView {
    var bottom: CGFloat { return frame.origin.y + bounds.height }
    var leftSide: CGFloat { return frame.origin.x }
    var rightSide: CGFloat { return frame.origin.x + bounds.width }
    var top: CGFloat { return frame.origin.y }
    
    func set(mode: EditableState = .normal, defaultBackgroundColor: UIColor? = .white, defaultBorderColor: CGColor? = UIColor.clear.cgColor, showDefaultBorder: Bool = false, animated: Bool = true) {
        UIView.animate(withDuration: ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
                        var newBackgroundColor: UIColor? = defaultBackgroundColor
                        var newBorderColor: CGColor? = defaultBorderColor
                        var newBorderWidth: CGFloat = showDefaultBorder ? 1.0 : 0.0
                        
                        if mode == .editing {
                            newBorderColor = UIColor.Naat.orange.cgColor
                            newBorderWidth = 1.0
                            newBackgroundColor = .white
                        }
                        else if mode == .error {
                            newBorderColor = UIColor.Naat.red.cgColor
                            newBorderWidth = 1.0
                            newBackgroundColor = .white
                        }
                        
                        self.layer.borderColor = newBorderColor
                        self.layer.borderWidth = newBorderWidth
                        self.backgroundColor = newBackgroundColor
                        
                        if let textField = self as? CustomUITextField {
                            if mode == .normal {
                                textField.font = textField.defaultFont
                                textField.textColor = textField.defaultTextColor
                            }
                            else {
                                textField.font = UIFont(name: "Roboto-Italic", size: (textField.font?.pointSize)!)
                                textField.textColor = mode == .error ? UIColor.Naat.red : UIColor.Naat.orange
                            }
                        }
                        else if let textView = self as? CustomUITextView {
                            if mode == .normal {
                                textView.font = textView.defaultFont
                                textView.textColor = textView.defaultTextColor
                            }
                            else {
                                textView.font = UIFont(name: "Roboto-Italic", size: (textView.font?.pointSize)!)
                                textView.textColor = mode == .error ? UIColor.Naat.red : UIColor.Naat.orange
                            }
                        }
        }, completion: nil)
    }
    
    func setVisible(_ visible: Bool, animated: Bool, onComplete: (() -> Void)?) {
        if visible == true {
            alpha = 0.0
            isHidden = false
        }
        
        UIView.animate(withDuration: visible ? ANIMATION_DEFAULT_DURATION_ENTER : ANIMATION_DEFAULT_DURATION_EXIT,
                       delay: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        self.alpha = visible ? 1.0 : 0.0
        }) { (finished) in
            if visible == false {
                self.isHidden = true
            }
            
            if onComplete != nil {
                onComplete!()
            }
        }
    }
    
    func fitHeightToContent(horizontalSpacing: CGFloat, verticalSpacing: CGFloat, ignore: [UIView]) {
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        var maxY: CGFloat = 0.0
        
        autoresizesSubviews = false
        
        for view in subviews {
            if view.isHidden == false && ignore.contains(view) == false {
                if (view.bounds.width + x) > bounds.width {
                    x = 0.0
                    y += (view.bounds.height + verticalSpacing)
                }
                
                view.frame.origin.x = x
                view.frame.origin.y = y
                x += view.bounds.width + horizontalSpacing
                
                if view.bottom > maxY {
                    maxY = view.bottom
                }
            }
        }
        
        frame.size.height = maxY
    }
    
    func isAffected(by filters: [[ViewFilterParameter:Any]]) -> Bool {
        for filter in filters {
            for (property, value) in filter {
                if property == .equals && self.isEqual(value) {
                    return true
                }
                
                if property == .height {
                    let height = (value as! NSNumber).floatValue
                    if self.bounds.height == CGFloat(height) {
                        return true
                    }
                }
                
                if property == .width {
                    let width = (value as! NSNumber).floatValue
                    if self.bounds.width == CGFloat(width) {
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    @discardableResult func reorder(direction: ReOrderDirection = .vertical, resize: Bool = true, ignoreFilters: [[ViewFilterParameter:Any]] = [], offset: ViewOffset = ViewOffset(bottom: 0.0, left: 0.0, right: 0.0, top: 0.0), spacing: CGFloat = 0.0, initialMargin: CGFloat = 0.0) -> CGFloat {
        var totalSize: CGFloat = 0.0
        
        if direction == .vertical {
            totalSize += offset.top
        }
        else {
            totalSize += offset.left
        }
        
        for view in self.subviews {
            if view.isAffected(by: ignoreFilters) == false {
                if initialMargin >= 0.0 {
                    if direction == .horizontal {
                        view.frame.origin.y = initialMargin
                    }
                    else {
                        view.frame.origin.x = initialMargin
                    }
                }
                
                if direction == .vertical {
                    view.frame.origin.y = totalSize
                    
                    if view.isHidden == false {
                        totalSize += view.bounds.height
                        
                        if view.bounds.height > 0.0 {
                            totalSize += spacing
                        }
                    }
                }
                else if direction == .horizontal {
                    view.frame.origin.x = totalSize
                    if view.isHidden == false {
                        totalSize += view.bounds.width
                        
                        if view.bounds.width > 0.0 {
                            totalSize += spacing
                        }
                    }
                }
            }
        }
        
        if totalSize < 0.0 {
            totalSize = 0.0
        }
        
        if totalSize > 0.0 && spacing > 0.0 {
            totalSize -= spacing
        }
        
        totalSize += offset.bottom
        
        if resize {
            if direction == .horizontal {
                frame.size.width = totalSize
            }
            else {
                frame.size.height = totalSize
            }
        }
        
        return totalSize
    }
    
    func addDropShadow(_ color: UIColor = UIColor.black, offset: CGSize = CGSize(width: 0.0, height: 2.0), opacity: CGFloat = 0.5, radius: CGFloat = 3.0) {
        let shadowPath = UIBezierPath(rect: CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height))
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity.f
        layer.shadowPath = shadowPath.cgPath
    }
    
    func updateLayout(animated: Bool) {
        UIView.animate(withDuration: animated ? ANIMATION_DEFAULT_DURATION_EXIT : 0.0, delay: 0, options: [.beginFromCurrentState, .allowAnimatedContent, .curveEaseInOut], animations: {
            self.layoutIfNeeded()
        }, completion: nil)
    }
}
