//
//  AttributedString.swift
//  Buckets
//
//  Created by Miyagui on 8/2/17.
//  Copyright © 2017 Buclets LLC. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    @discardableResult func attributedFont(text: String, color: UIColor = UIColor.Naat.dark, size: CGFloat = 14.0, bold: Bool = false) -> NSMutableAttributedString {
        var fontName = "Helvetica"
        if bold {
            fontName += "-Bold"
        }
        
        self.append(NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.font : UIFont(name: fontName, size: size)!, NSAttributedStringKey.foregroundColor : color]))
        return self
    }
    
    @discardableResult func image(_ urlSource: String?) -> NSMutableAttributedString {
        if urlSource != nil && urlSource?.isEmpty == false {
            let attachment = NSTextAttachment()
            attachment.bounds = CGRect(x: 0.0, y: 0.0, width: 20.0, height: 20.0)
            
            let imageContainer = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0))
            imageContainer.sd_setImage(with: URL(string: urlSource!)) { (image, error, cacheType, url) in
                attachment.image = image
            }
            
            let extra = NSAttributedString(attachment: attachment)
            self.append(extra)
        }
        
        return self
    }
    
    @discardableResult func link(text:String, url: String, size: CGFloat)->NSMutableAttributedString {
        let linkString = NSMutableAttributedString(string: text, attributes:[NSAttributedStringKey.font :  UIFont(name: "Helvetica", size: size)!, NSAttributedStringKey.foregroundColor : UIColor.Naat.orange])
        linkString.addAttribute(NSAttributedStringKey.link, value: url, range: NSRange(location: 0, length: text.lengthOfBytes(using: .utf8)))
        self.append(linkString)
        
        return self
    }
}

extension String {
    func isValidRFC() -> Bool {
        return self.range(of: "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))$", options: .regularExpression, range: nil, locale: nil) != nil
    }
    
    func height(maxWidth: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: maxWidth, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
}
